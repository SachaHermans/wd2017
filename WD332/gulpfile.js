var gulp   = require( 'gulp' ),
    sass   = require( 'gulp-sass' ),
    cssmin = require( 'gulp-cssmin' ),
    rename = require( 'gulp-rename' );


gulp.task( 'styles', function () {
	return gulp.src( 'sass/**/*.scss' )
	           .pipe( sass().on( 'error', sass.logError ) )
	           .pipe( gulp.dest( './css/' ) )
} );

gulp.task( 'styles', function () {
	gulp.src( 'sass/**/*.scss' )
	    .pipe( sass().on( 'error', sass.logError ) )
	    .pipe( cssmin() )
	    .pipe( rename( { suffix : '.min' } ) )
	    .pipe( gulp.dest( './css/' ) )
} );

gulp.task( 'default', function () {
	gulp.watch( 'sass/**/*.scss', ['styles'] );
} );