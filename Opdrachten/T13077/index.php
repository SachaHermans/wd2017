<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// declaratie van de variabele voor het geven van een return

$titel='Het fluitketeltje';
$omschrijving='Meneer is niet thuis en mevrouw is niet thuis, het keteltje staat op het kolenfornuis, de hele familie is uit, en het fluit en het fluit en het fluit: túúúút';
$date=date('l j F Y');

function toon_bericht($bericht_titel, $bericht_omschrijving, $bericht_date){
    $new_line = "<br>\n";
    echo '<h2>' . $bericht_titel . '</h2>';
    echo '<p>' . $bericht_omschrijving . $new_line . $bericht_date . '</p>';
}

function get_h1(){
    $h1=>getElementsByTagName('h1');
    foreach ($h1){
        echo $h1;
    }
}
get_h1();
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <?php   $titel="College of MultiMedia | PHP MySQL";?>
	

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>

<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PHP MySQL</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="pages/about.php           ">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>
    
    <div class="jumbotron">
        <div class="container">
            <?php   $h1="hello, world!";
                    echo "<h1>" . ucfirst($h1) . "</h1>";
            ?>

            <p>Dit is een template voor een eenvoudige website. <br>
				Het omvat een grote callout genoemd de heldenEenheid en drie ondersteunende stukken of inhoud. Gebruik het as een uitgangspunt om iets</p>

            <p><a href="pages/about.php" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
               <h1>Dikkertje Dap</h1>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            
        </div>
        
        <hr>
        
        <footer>
            <p>&copy; 2017 College of MultiMedia | Mail ons</p>
        </footer>
    </div>
    <!-- /container -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
