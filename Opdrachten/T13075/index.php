<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

$newLine = "<br>\n";

//Bereken de uitkomst van de sommen en stop deze in de variabele
$oplossing1 = (-2+4)*(25*9+26)+25*6;
$oplossing2 = -2+4*25*9+26+25*6;
$oplossing3 = 9*758;
$oplossing4 = 14/58;
$oplossing5 = 345*(2+7);
$oplossing6 = 40-(3*3);
$oplossing7 = 2**12;
$oplossing8 = (2.4**2)+(5*1.9);
$oplossing9 = 100+(9.5/33);

//Toon de sommen en de uitkomst
echo '(-2+4)x(25x9+26)+25x6 = ' . $oplossing1, $newLine;
echo '-2+4x25x9+26+25x6 = ' . $oplossing2, $newLine;
echo '9x758 = ' . $oplossing3, $newLine;
echo '14/58 = ' . $oplossing4, $newLine;
echo '345x(2+7) = ' . $oplossing5, $newLine;
echo '40-(3x3) = ' . $oplossing6, $newLine;
echo '2 tot de macht 12 = ' . $oplossing7, $newLine;
echo '(2,4 tot de macht 2)+(5x1,9) = ' . $oplossing8, $newLine;
echo '100+(9,5/33) = ' . $oplossing9, $newLine;

?>