<?php
$br = "<br>\n";
$mysqli = mysqli_connect ('localhost','root','root','wd09122017');

if (mysqli_connect_errno()){
	echo 'Connection failed: ' . mysqli_connect_error();
	die();
}

$query = 'SELECT id, name FROM fruit ORDER by id' ;
# echo $query;

# PREPARE en CONTROLEER
if (! $prep_query = $mysqli->prepare($query)){
	throw new Exception ('Prepare failed: (' . $mysqli->errno . ') ' . $mysqli->error);
}

# VOER UIT en ONTROLEER
if (! $prep_query-> execute()){
	throw new Exception ('Execute failed: (' . $mysqli->errno . ') ' . $mysql->error);
}

# OPHALEN GEGEVENS en IN OBJECT STOPPEN
$result_object = $prep_query->get_result();
#var_dump($result_object);

# CONTROLEER of er resultaten zijn
if (empty($result_object->num_rows)){
	die('Niets gevonden');
}
# echo 'Iets gevonden';

# VERTAAL naar Associative ARRAY

?>
<form method="get">
<select name="fruit">
	<?php
	while ($fruitItem = $result_object->fetch_assoc()){
		echo '<option value=' . $fruitItem['id'] . '>' . $fruitItem['name'] . '</option>';
	}
	?>
</select>
<button>Filter</button>
</form>
<?php
$id=$_GET['fruit'];
$query = 'SELECT description, amount 
FROM stock
WHERE fruit_id=?';
# echo $query;

# PREPARE en CONTROLEER
if (! $prep_query = $mysqli->prepare($query)){
	throw new Exception ('Prepare failed: (' . $mysqli->errno . ') ' . $mysqli->error);
}
$prep_query->bind_param('i', $fruit_id);
# VOER UIT en ONTROLEER
if (! $prep_query-> execute()){
	throw new Exception ('Execute failed: (' . $mysqli->errno . ') ' . $mysql->error);
}

# OPHALEN GEGEVENS en IN OBJECT STOPPEN
$result_object = $prep_query->get_result();
#var_dump($result_object);

# CONTROLEER of er resultaten zijn
if (empty($result_object->num_rows)){
	die('Niets gevonden');
}

while ($stockItem = $result_object->fetch_assoc()){
		echo $stockItem['description'] . $stockItem['amount'] . "<br>\n";
	}