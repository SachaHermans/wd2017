<?php
$br = "<br>\n";
$mysqli = mysqli_connect ('localhost','root','root','wd09122017');

if (mysqli_connect_errno()){
	echo 'Connection failed: ' . mysqli_connect_error();
	die();
}

# echo 'Ik heb verbinding';

$query = 'SELECT name, stock.description, stock.amount FROM fruit JOIN stock on fruit.id= stock.fruit_id ORDER BY name DESC, stock.description' ;
# echo $query;

# PREPARE en CONTROLEER
if (! $prep_query = $mysqli->prepare($query)){
	throw new Exception ('Prepare failed: (' . $mysqli->errno . ') ' . $mysqli->error);
}

# VOER UIT en ONTROLEER
if (! $prep_query-> execute()){
	throw new Exception ('Execute failed: (' . $mysqli->errno . ') ' . $mysql->error);
}

# OPHALEN GEGEVENS en IN OBJECT STOPPEN
$result_object = $prep_query->get_result();
#var_dump($result_object);

# CONTROLEER of er resultaten zijn
if (empty($result_object->num_rows)){
	die('Niets gevonden');
}
# echo 'Iets gevonden';

# VERTAAL naar Associative ARRAY
while ($fruitItem = $result_object->fetch_assoc()){
	echo $fruitItem['name'] . ' ' . $fruitItem['description'] . ' = ' . $fruitItem['amount'] . $br;
}