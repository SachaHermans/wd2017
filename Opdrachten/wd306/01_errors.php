    <?php
    // laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);
    
    $voornaam = ('Sacha');
    $achternaam = ('Hermans');
    
    echo "Mijn naam is $voornaam $achternaam<br>";
    echo 'Mijn naam is ' . $voornaam . ' ' . $achternaam . '<br>';
    echo 'Mijn naam is ';
	echo $voornaam;
	echo ' ';
	echo $achternaam;