<h1>Arrays.</h1>
<h2>Opdracht 1</h2>
Een indexed array aan met minimaal 5 auto merken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<?php
$indexArray = ['saab','simca','kia','honda','toyota'];
print_r($indexArray);
?>

<h2>Opdracht 2</h2>
Maak nu een assiocatieve array met 5 automerken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<?php
$assoArray =['merk' => 'saab',
			 'merk2' => 'simca',
			 'merk3' => 'kia',
			 'merk4' => 'honda',
			 'merk5' => 'toyota'];
print_r($assoArray);
?>

<h2>Opdracht 3</h2>
Gebruik de bovenstaande array<br>
Voeg per merk 3 types toe<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 4</h2>
Gebruik de bovenstaande array<br>
Geef per type aan wat de prijs is<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 5</h2>
Pas de weergave van de laaste array zo aan dat het volgende zichtbaar is<br>
De <b>Volkswagen</b> van het type <b>Polo</b> kost <b>€ 13.333,00</b><br>
De <b>Volkswagen</b> van het type <b>Up</b> kost <b>€ 9.999,00</b><br>
De <b>Volkswagen</b> van het type <b>Golf</b> kost<b>€ 39.999,00</b><br>
De <b>BMW</b> van het type <b>3</b> kost <b>€ 13.333,00</b><br>
etc.