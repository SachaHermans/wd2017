<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Over mij</title>

</head>

<body>
	<div>
		<img src="images/volkswagen.jpg"  alt="Over mij" width="960" height="639" title="Over mij">
		<h1>Volkswagen Type 2</</h1>
		<p>The Volkswagen Type 2, known officially (depending on body type) as the Transporter, Kombi or Microbus, or, informally, as the Bus (US) or Camper (UK), is a forward control panel van introduced in 1950 by the German automaker Volkswagen as its second car model. Following – and initially deriving from Volkswagen's first model, the Type 1 (Beetle) – it was given the factory designation Type 2.[2]

As one of the forerunners of the modern cargo and passenger vans, the Type 2 gave rise to forward control competitors in the United States in the 1960s, including the Ford Econoline, the Dodge A100, and the Chevrolet Corvair 95 Corvan, the latter adopting the Type 2's rear-engine configuration.

European competition included the 1947–1981 Citroën H Van, the 1959–1980 Renault Estafette (both FF layout), and the 1953–1965 FR layout Ford Transit.

Japanese manufacturers also introduced similar vehicles, such as the Nissan Caravan, Toyota LiteAce and Subaru Sambar.

Like the Beetle, the van has received numerous nicknames worldwide, including the "microbus", "minibus",[3] and, because of its popularity during the counterculture movement of the 1960s, Hippie van/wagon, and still remains iconic for many hippies today.

Brazil contained the last factory in the world that produced the T2 series of Type 2, which ceased production on December 31, 2013, due to the introduction of more stringent safety regulations in the country.[4] This (after the 2002 termination of its T3 successor in South Africa) marked the end of the era of rear-engine Volkswagens manufactured, which originated in 1935 with their Type 1 prototypes.</p>
	
	</div>
</body>
</html>
