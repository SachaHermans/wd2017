<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

//functie voor het tonen van berichten
function toon_bericht($bericht_titel, $bericht_omschrijving, $bericht_date){
    $new_line = "<br>\n";
    echo '<h2>' . $bericht_titel . '</h2>';
    echo '<p>' . $bericht_omschrijving . $new_line . $new_line . $bericht_date . '</p>';
}