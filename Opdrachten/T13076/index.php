<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// declaratie van de variabele voor het geven van een return
$newLine = "<br>\n";
// declaratie van de variabele voor het getal van de tafel
$tafel = 12;
// declaratie van de variabele voor het maximale aantal regels dat getoond mag worden (33+67=100)
$eindGetal = 100;
// declaratie van de variabele voor het tellen van het aantal weergegeven regels
$aantalMaal = 0;;

//loop voor het 67 keer weergeven van de regel
for ($maalGetal = 33; $maalGetal <= $eindGetal; $maalGetal++ ){
        // conditie om de tafelregel niet weer te geven als het resultaat 420 is of het maalgetal 41
        if ($maalGetal*$tafel==420 || $maalGetal==41){
            continue;
        // conditie om te stoppen met de loop als het resultaat groter is dan 760
        } elseif ($maalGetal*$tafel>790){
            break;
        } else {
            // tonen van de tafelregel inclusief return
            echo "&nbsp $maalGetal x $tafel = " . $maalGetal*$tafel, $newLine;
            // Ophogen variabele voor tellen van het aantal keer dat de tafelregel wordt getoond
            $aantalMaal++;
        }
}
//tonen van het aantal keer dat de tafelregel is getoond
echo "Het aantal regels is " . $aantalMaal;
?>