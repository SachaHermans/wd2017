<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// declaratie van de variabele voor het geven van een return
$newLine = "<br>\n";
echo 'Verbinding maken' . $newLine;

//query voor lijst met merken, landen en auto's

$auto_lijst = 'SELECT naam, land, autos.title, autos.type, autos.kleur, autos.brandstof, autos.zitplaatsen, autos.prijs
FROM merken, landen, autos
WHERE merken.merkland_id = landen.land_id AND autos.merk_id = merken.merk_id
ORDER BY naam';

//verbinding maken met database

$database_verbinding = mysqli_connect ('localhost', 'root', 'root', 'T131312');

//controle of verbinding gelukt is

if (mysqli_connect_errno()){
            echo 'Verbinding mislukt met foutmelding: ' . mysqli_connect_error; 
} else
    echo 'Verbinding gelukt' . $newLine;

//voorbereiden van de query inclusief controle op fouten

if ($prepare_gelukt = $database_verbinding->prepare($auto_lijst)){
    echo 'Voorbereiding gelukt' . $newLine;
} else
    throw new exception('Voorbereiding mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);

//uitvoeren van de query inclusief controle op fouten

if ($prepare_gelukt->execute()){
    echo 'Uitvoering gelukt' . $newLine;
} else
    throw new exception('Uitvoering mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);

//Ophalen van de gegevens

$prepare_result = $prepare_gelukt->get_result();

//Controle of er gegevens gevonden zijn

if (empty($prepare_result->num_rows)){
    echo 'Geen gegevens gevonden';
}

$row_cnt = $prepare_result->num_rows;
echo  'Het aantal regels is' . $row_cnt . $newLine;

while ($autos = mysqli_fetch_assoc($prepare_result)){
	if (!isset( $_GET['zoek'] ) || isset($_GET['reset'])){
	    	echo 'Naam:' . $autos['naam'] . 'Land:' . $autos['land'] . 'Titel:' . $autos['title'] . 'Type:' . $autos['type'] . 'Kleur:' . $autos['kleur'] . 'Brandstof:' . $autos['brandstof'] . 'Zitplaatsen:' . $autos['zitplaatsen'] . 'Prijs:' . $autos['prijs'] . $newLine;
	} elseif (($_GET['zoek']== $autos['naam']) || ($_GET['zoek']== $autos['type'])){
			echo 'Naam:' . $autos['naam'] . 'Land:' . $autos['land'] . 'Titel:' . $autos['title'] . 'Type:' . $autos['type'] . 'Kleur:' . $autos['kleur'] . 'Brandstof:' . $autos['brandstof'] . 'Zitplaatsen:' . $autos['zitplaatsen'] . 'Prijs:' . $autos['prijs'] . $newLine;
	} else {
		echo "Niets gevonden";
		break;
	}
}
?>
<form method="get">
  <input type="text" name="zoek"><br>
  <button>Zoek</button><button name="reset">Reset</button>
</form>