<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// declaratie van de variabele voor het getal van de tafel
$tafel = 5;

// tonen om welke tafel het gaat

echo "De tafel van $tafel<br><br>\n";

/* tonen van de verschillende vermenigvuldigingen van de tafel
    elk op een aparte regel 
    met een spatie voor 1 tot en met negen voor de uitlijning
*/

echo "&nbsp 1 x $tafel = " . 1*$tafel . "</br>\n";
echo "&nbsp 2 x $tafel = " . 2*$tafel . "</br>\n";
echo "&nbsp 3 x $tafel = " . 3*$tafel . "</br>\n";
echo "&nbsp 4 x $tafel = " . 4*$tafel . "</br>\n";
echo "&nbsp 5 x $tafel = " . 5*$tafel . "</br>\n";
echo "&nbsp 6 x $tafel = " . 6*$tafel . "</br>\n";
echo "&nbsp 7 x $tafel = " . 7*$tafel . "</br>\n";
echo "&nbsp 8 x $tafel = " . 8*$tafel . "</br>\n";
echo "&nbsp 9 x $tafel = " . 9*$tafel . "</br>\n";
echo "10 x $tafel = " . 10*$tafel . "</br>";


?>