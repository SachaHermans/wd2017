<?php
require_once( 'Voertuig.php' );

class Auto extends Voertuig{
	public function __construct(){
		$this->setNaam('Auto');
		$this->setBanden(4);
	}

protected $_zitplaatsen;

public function getZitplaatsen(){
		return $this->_zitplaatsen;
	}
	
	public function setZitplaatsen($zitplaatsen){
		if(is_int($zitplaatsen)){
			return;
		}
		$this->_zitplaatsen = ($zitplaatsen);
	}
	
}