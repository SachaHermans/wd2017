<?php
require_once( 'Voertuig.php' );

class Fiets extends Voertuig{
	public function __construct(){
		$this->setNaam('Fiets');
		$this->setBanden(2);
	}
}