<?php

class Voertuig {

	protected $_banden;
	protected $_naam;
	
	public function __construct ($naam, $banden=2) {
		$this->setBanden($banden);
		$this->setNaam($naam);
	}
	
	public function getBanden(){
		return $this->_banden;
	}
	
	public function setBanden($aantal){
		if(! is_int($aantal)){
			return;
		}
		$this->_banden = ($aantal);
	}
	
	public function getNaam(){
		return $this->_naam;
	}
	
	public function setNaam($naam){
		if(is_int($naam)){
			return;
		}
		$this->_naam = ($naam);
	}
	
	public function showInfo(){
		echo 'De naam van mijn object is: ' . $this->getNaam() . '<br>';
		echo 'Het aantal banden van mijn object: ' . $this->getBanden() . '<br>';
	}
}
