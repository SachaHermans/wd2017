<?php
require_once( 'Voertuigen/Voertuig.php' );
require_once( 'Voertuigen/Auto.php' );
require_once( 'Voertuigen/Fiets.php' );

$vliegtuig = new Voertuig('Vliegtuig',3);
$step = new Voertuig('Step');
$auto = new Auto();
$fiets = new Fiets();

$vliegtuig->showInfo();
$step->showInfo();
$auto->showInfo();
$fiets->showInfo();
