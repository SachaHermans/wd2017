<?php
//verbinding met database wordt gemaakt door de require_once in settings.php van database.php

//query voor ophalen van nieuwsberichten
$nieuws_query= 'SELECT id, titel, publicatie_datum, content
FROM nieuwsberichten
ORDER BY publicatie_datum DESC';

//voorbereiden van de query inclusief controle op fouten

if ( ! $prepare_gelukt = $database_verbinding->prepare($nieuws_query)){
    throw new exception('Voorbereiding mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);
}
    
//uitvoeren van de query inclusief controle op fouten

if ( ! $prepare_gelukt->execute()){
    throw new exception('Uitvoering mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);
} 
    
//Ophalen van de gegevens

$prepare_result = $prepare_gelukt->get_result();

//Controle of er gegevens gevonden zijn

if (empty($prepare_result->num_rows)){
    die('Geen gegevens gevonden');
}
include_once VIEWS_PATH . 'News/viewberichten.php';