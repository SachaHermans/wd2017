<?php
class BerichtController {
	
	protected $_db;
	
	public function __construct($database){
		$this->_db=$database;
	}
	
	public function insertBericht($titel, $content){
		$query="INSERT INTO `nieuwsberichten` (`titel`,`content`) VALUES ('$titel', '$content')";		
		$this->_db->setBericht($query);
	}
	
	public function setBericht($titel, $content){
		
	//voorbereiden van de query inclusief controle op fouten
	if ( ! $prepare_bericht = $this->_db->prepare("INSERT INTO `nieuwsberichten` (`titel`,`content`) VALUES (?, ?)")){
    	throw new exception('Voorbereiding mislukt: ' . $this->_verbinding->errno . ')' . $this->_verbinding->error);
		}
		
	if ( ! $prepare_bericht->bind_param('ss', $titel, $content) ){
		throw new exception('Voorbereiding mislukt: ' . $prepare_bericht->errno . ')' . $prepare_bericht->error);
		}

		
	//uitvoeren van de query inclusief controle op fouten
	if ( ! $prepare_bericht->execute()){
		throw new exception('Uitvoering mislukt: ' . $this->_verbinding->errno . ')' . $this->_verbinding->error);
		}	 
	}
}