<?php
// declaratie van de variabele voor het geven van een return
$newLine = "<br>\n";

if (isset($_POST['inlog_naam']) && ($_POST['inlog_ww'])) {
	$_SESSION['inlog_naam']=($_POST['inlog_naam']);
	$_SESSION['inlog_ww']=($_POST['inlog_ww']);
	logged_in();
}

if (isset($_POST['loguit'])){
	session_unset();
	session_destroy();
}

if (isset($_SESSION['ingelogd']) && 'true'==($_SESSION['ingelogd'])){ ?>
	<form class="navbar-form navbar-right" action="" method="post">
		<div class="form-group">
			<div class="form-control"><?php echo $_SESSION['inlog_naam']?></div>
		</div>
		<input type="hidden" name="loguit" value="loguit"><button type="submit" class="btn btn-success">Sign out</button>
	</form><?php
} else { ?>
	<form class="navbar-form navbar-right" action="" method="post">
		<div class="form-group">
    		<input type="text" name="inlog_naam" placeholder="Email" class="form-control">
    	</div>
    	<div class="form-group">
    		<input type="password" name="inlog_ww" placeholder="Password" class="form-control">
    	</div>
		<button type="submit" class="btn btn-success">Sign in</button>
	</form><?php
}