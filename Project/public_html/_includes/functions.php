<?php
//functie voor het tonen van berichten
function toon_bericht($bericht_titel, $bericht_omschrijving, $bericht_date){
    $new_line = "<br>\n";
    echo '<h2>' . $bericht_titel . '</h2>';
    echo '<p>' . $bericht_omschrijving . $new_line . $new_line . $bericht_date . '</p>';
}

//functie voor de controle of er een gebruiker ingelogd is
function logged_in() {
	if ((''!==$_SESSION['inlog_naam']) || (''!==$_SESSION['inlog_ww']))
		$_SESSION['ingelogd']=true;
}