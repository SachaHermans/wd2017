<?php

//laden van instellingen en functies

require_once '../../Library/Settings/config.php';
require_once CONTROLLERS_PATH . 'functions.php';

$page_active = basename($_SERVER["PHP_SELF"], ".php");


//laden van de header
include_once VIEWS_PATH . 'Main/header.php';
?>

<div class="container">
		<?php
		if (isset($_GET['id'])){
			include_once CONTROLLERS_PATH . 'nieuwsbericht.php';
		}
		include_once VIEWS_PATH . 'News/viewbericht.php';
				
		if (isset($_POST['titel']) && ($_POST['content'])){
			require_once SERVICES_PATH . 'DatabaseVerbinding.php';
			$database=new DatabaseVerbinding();
						
			require_once CONTROLLERS_PATH . 'BerichtController.php';
			$bericht=new BerichtController($database);
			
			//$bericht->insertBericht($_POST['titel'], $_POST['content']);
			$bericht->setBericht($_POST['titel'], $_POST['content']);
		}

	
		include_once VIEWS_PATH . 'Main/footer.php';