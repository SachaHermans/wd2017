<?php
class BerichtController {
	
	private $_db;
	
	public function __construct($database){
		$this->_db=$database;
	}
	
	public function insertBericht(){
		$query="INSERT INTO `nieuwsberichten` (`titel`,`content`, `publicatie_datum`, `update_datum`) VALUES ('titel', 'content', 'pubDatum', 'upDatum')";		
		//voorbereiden van de query inclusief controle op fouten
		if ( ! $prepare_bericht = $this->_db->prepare($query)){
    		throw new exception('Voorbereiding mislukt: ' . $this->_db->errno . ')' . $this->_db->error);
			}
		
		//uitvoeren van de query inclusief controle op fouten

		if ( ! $prepare_bericht->execute()){
			throw new exception('Uitvoering mislukt: ' . $this->_db->errno . ')' . $this->_db->error);
		}	 
	}
}