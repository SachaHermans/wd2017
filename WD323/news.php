<?php

class NewsModel {

	protected $_title;
	protected $_content;
	protected $_datum;
	protected $_actief;
	
	public function __construct($titel='default titel', $content='default content', $datum='default datum', $actief='default actief'){
		$this->_title = $titel;
		$this->_content = $content;
		$this->_datum = $datum;
		$this->_actief = $actief;
	}
	public function showNews(){
		echo $this->_title;
		echo "<br>\n";
		echo $this->_content;
		echo "<br>\n";
		echo $this->_datum;
		echo "<br>\n";
		echo $this->_actief;
		echo "<br><hr>\n";
	}

	public function getNewsArray(){
		return [
			'title' => $this->_title,
			'content' => $this->_content,
			'datum' => $this->_datum,
			'actief' => $this->_actief,
		];
	}
}

function functionController(){
$nieuwsbericht1 = new NewsModel ('Bericht 1', 'tekst', 'vandaag', 'ja' );
//$nieuwsbericht1->showNews();

$nieuwsbericht2 = new NewsModel ('Bericht 2', 'geen tekst', 'morgen', 'nee');
//$nieuwsbericht2->showNews();

$nieuwsbericht3 = new NewsModel ();
//$nieuwsbericht3->showNews();

	$berichtenArray = [
						15 => $nieuwsbericht1->getNewsArray(), 
						23 => $nieuwsbericht2->getNewsArray(), 
						30 => $nieuwsbericht3->getNewsArray()
					   ];

	header('Content-type: application/json');

	if (!isset($_GET['id'])) {
		echo json_encode($berichtenArray);
		return;
		}
	if (!isset($berichtenArray[$_GET['id']])){
		echo('Bericht niet gevonden');
		return;
		}
	echo json_encode ($berichtenArray[$_GET['id']]);	

}

functionController();