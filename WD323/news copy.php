<?php
/**
 * Nieuws bericht model ( een Class / Object ) 
 */
class NewsModel {

	protected $_title;
	protected $_content;
	protected $_date;
	protected $_active;
	
	/**
	 * Default functie die aangeroepen wordt bij de call: new News();
 	 * Hier kan ik bijvoorbeeld de titel etc. defineren
 	 */
	public function __construct( $titel='default titel', $bericht='leeg', $datum='nu', $zichtbaar=1 ) {
		$this->_title 	= $titel;
		$this->_content = $bericht;
		$this->_date 	= $datum;
		$this->_active 	= $zichtbaar;
	}
	
	/**
	 * Laat het bericht zien
	 */	
	public function showNews() {
		echo 'Dit is mijn nieuws mijn titel is: ' . $this->_title;
		echo "<br><hr>\n";
	}
	
	/**
	 * Public function die de inhoud van dit NewsModel terug geeft.
	 * Geen echo, dus ik zie niets in mijn browser
	 */
	public function getNewsModelArray() {
		return [ 
					'header' 	=> ucFirst( $this->_title ), 
					'tekst'		=> $this->_content,
					'datum'		=> $this->_date,
					'zichtbaar'	=> $this->_active
				];
	}
	
}


/**
 * Controller functie, dit zou bijvoorbeeld een controller object kunnen zijn
 */
function controllerFunction() {
	/*
	 * Maak een aantal nieuw bericht aan
	 */
	$nieuwsbericht1 = new NewsModel( 'Bericht 1', 'tekstje', 'vandaag', 1 );
	$nieuwsbericht2 = new NewsModel( 'bericht 2', 'bericht tekst', 'morgen', 0 );
	$nieuwsbericht3 = new NewsModel( );
	
	/*
	 * Maak een array aan en vul deze met mijn berichten
	 * Van elk bericht geef ik een array met de waardes terug
	 */
	$berichtenArray = [ 
						15 => $nieuwsbericht1->getNewsModelArray(), 
						23 => $nieuwsbericht2->getNewsModelArray(), 
						55 => $nieuwsbericht3->getNewsModelArray() 
					  ];

	/*
	 * Zorg dat de output altijd een JSON document is
	 */
	header('Content-type: application/json');

	/**
	 * Controleer of de parameter id meegegeven is in de URL 
	 * bij het aanroepen van de pagina
	 * Als er geen id in de url meegegeven is wil ik alle berichten zien
	 */
	if ( ! isset( $_GET['id'] ) ) {
		echo json_encode( $berichtenArray );
		/*
		 * zorg dat het script niet verder gaat
		 */
		return;
	}
	
	/*
	 * Op dit moment weet ik dat de parameter id voorkomt in de url
	 */
	if( ! isset( $berichtenArray[ $_GET['id'] ] ) ) {
		echo 'ID niet gevonden';
		/*
		 * zorg dat het script niet verder gaat
		 */
		return;
	}

	/*
	 * Nu weet ik dat de id voorkomt in de request url 
	 * én dat deze id voorkomt als key in de berichten array
	 */
	echo json_encode( $berichtenArray[ $_GET['id'] ] );

}



controllerFunction();














