<?php

namespace App\Http\Controllers;

use App\News;

class NewsController extends Controller
{
    /**
     * Laat de laatste 5 nieuwsberichten zien
     * Het model zorgt voor de query,
     * hier hoef ik alleen te zeggen hoeveel berichten ik wil
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $messages = News::Latest(5);
		$test = 'hoi';
		
        return view('news.index',
            compact('messages', 'test')
        );
    }

    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
		$news=News::Active($slug);
        
        return view('news.show',
            compact('news')
        );
    }

    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
    	$news = new News;
    	 if (session()->hasOldInput()){
        	$news->title= session()->getOldInput('title');
        	$news->publish_date= session()->getOldInput('publish_date');
        	$news->body= session()->getOldInput('body');
        }
        
        return view('news.create', compact('news'));
    }
    
    public function store (){
    	#dd(request()->all());
    	
    	$this->validate ( request(), [
    		'title'			=> 'required|min:2|max:150',
    		'publish_date'	=> 'required|date',
    		'body'			=> 'required',
    	]);
    	News::create( request(['title', 'publish_date', 'body']) );
	return redirect( '/news' );    	
    }
    
    public function delete ($news_id){
    
		News::find ($news_id)->delete();    
    	return redirect( '/news' )->with ('message', 'hij is weg');  	
    }
    
    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(News $news)
    {
        if (session()->hasOldInput()){
        	$news->title= session()->getOldInput('title');
        	$news->publish_date= session()->getOldInput('publish_date');
        	$news->body= session()->getOldInput('body');
        }
        
        return view('news.edit',
            compact('news')
        );
    }
    
    public function update ( News $news){
    	#dd(request()->all());
    	
    	$this->validate ( request(), [
    		'title'			=> 'required|min:2|max:150',
    		'publish_date'	=> 'required|date',
    		'body'			=> 'required',
    	]);
    	
    	$news->update(request(['title','publish_date','body']));
	
		return redirect( '/news/' . $news->slug )->with('message','Gelukt');    	
    }
    
}

















