<?php

namespace App\Http\Controllers;

use App\News;
use App\Page;

class PagesController extends Controller
{
    /**
     * Dit is de home pagina
     * waarbij je 3 nieuwsberichten ziet
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        # we kunnen ook hier gebruik maken van de Latest functie uit het Model
        $messages = News::Latest(3);
        $page     = Page::ActivePage('welcome');

        return view('page.index',
            compact('messages', 'page')
        );
    }

    /**
     * De page functie die een specifieke pagina laat zien
     *
     * @param $url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($url)
    {
        $page = Page::ActivePage($url);

        if (empty($page->title)) {
            return view('404');
        }

        return view('page.show',
            compact('page')
        );
    }
    public function profile()
    {
    	return view('page.profile');	
    
    }
}
