<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * @param        $query
     * @param string $url
     *
     * @return mixed
     */
    public function scopeActivePage($query, $url = 'welcome')
    {
        return $query->where(
            [
                ['active', '=', 1],
                ['url', '=', $url],
            ])->first();
    }
}
