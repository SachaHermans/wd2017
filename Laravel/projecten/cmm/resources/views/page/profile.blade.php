@extends( 'layout.master' )

@section('title')
    mijn Profiel
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>Hoi, dit is mijn profiel pagine</h1>
            <ul>
            	<li>ID: {{ Auth::user()->id}} </li>
            	<li>Naam: {{ Auth::user()->name}}</li>
            	<li>Email: {{ Auth::user()->email}}</li>
            	
            </ul>
        </div>
    </div>
@endsection
