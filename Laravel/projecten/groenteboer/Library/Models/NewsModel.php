<?php

namespace Library\Models;

/**
 * Mijn newsModel class, hierin worden alle gegevens per post opgeslagen
 * deze class is een news object
 */
class NewsModel
{
    // hier defineer ik een variabele die binnen deze gehele class gebruikt kan worden
    // deze variabelen zijn alleen maar binnen deze class te gebruiken en aan te passen
    // hierdoor zorg je ervoor dat je van tevoren weet wat de waarde van een variabele is
    protected $_id;
    protected $_category_id;
    protected $_post_url;
    protected $_user_id;
    protected $_title;
    protected $_intro;
    protected $_message;
    protected $_image_source;
    protected $_publish_date;
    protected $_creation_date;
    protected $_last_updated;
    protected $_category_title;
    protected $_user_name;


    /**
     * deze "__construct()" functie is standaard aanwezig
     * je hoeft hem niet aan te maken als je hem niet gebruikt
     * Deze functie is altijd public
     * Deze functie word altijd aangeroepen als de class gecreeërd is.
     * Deze functie gaan we nu wel aanmaken omdat ik tijdens het aanmaken
     *    van een nieuw nieuwsbericht een aantal variabelen direct wil instellen.
     * er wordt een key-value array meegegeven
     */
    public function __construct($defaultVars = [])
    {
        // loop door de meggegeven waardes heen
        foreach ($defaultVars as $prop => $value) {
            // zet de prop name om in een functie naam
            $prop   = str_replace(" ", "", ucwords(str_replace("_", " ", $prop)));
            $setter = "set" . ucfirst($prop);

            // controleer of deze methode ( funktie ) bestaat in deze class )
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }

        }
    }
    /* ----- START --- Getters and setters ----- */

    /**
     * getId
     * Return the var from this object or the default
     * @return Int
     */
    public function getId()
    {
        if ( ! empty($this->_id)) {
            return $this->_id;
        } else {
            return null;
        }
    }

    /**
     * setId
     * Sets the var with the given value
     *
     * @param Value Int
     */
    public function setId($value)
    {
        if (is_numeric($value)) {
            $this->_id = $value;
        }
    }

    /**
     * getCategoryId
     * Return the var from this object or the default
     * @return Category_id
     */
    public function getCategoryId()
    {
        if (isset($this->_category_id)) {
            return $this->_category_id;
        }
    }

    /**
     * setCategoryId
     * Sets the var with the given value
     *
     * @param Value Int
     */
    public function setCategoryId($value)
    {
        $this->_category_id = $value;
    }

    /**
     * getUserId
     * Return the var from this object or the default
     * @return User_id
     */
    public function getUserId()
    {
        if (isset($this->_user_id)) {
            return $this->_user_id;
        }
    }

    /**
     * setUserId
     * Sets the var with the given value
     *
     * @param Value Int
     */
    public function setUserId($value)
    {
        $this->_user_id = $value;
    }

    /**
     * getPostUrl
     * Return the var from this object or the default
     * @return Url
     */
    public function getPostUrl()
    {
        // zorg ervoor dat er altijd een url aanwezig is voor dit bericht
        if (empty($this->_post_url)) {
            $this->setPostUrl($this->getTitle());
        }

        if (isset($this->_post_url)) {
            return $this->_post_url;
        }
    }

    /**
     * setPostUrl
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setPostUrl($value)
    {
        $url             = str_replace([' ', '.'], ['_', ''], $value);
        $this->_post_url = strToLower(urlencode($url));
    }

    /**
     * getTitle
     * Return the var from this object or the default
     * @return Title
     */
    public function getTitle()
    {
        if (empty($this->_title)) {
            return 'Geen titel';
        }

        return ucwords($this->_title);
    }

    /**
     * setTitle
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setTitle($value)
    {
        $this->_title = $value;
    }

    /**
     * getIntro
     * Return the var from this object or the default
     * @return Intro
     */
    public function getIntro()
    {
        if (empty($this->_intro)) {
            return $this->getMessage();
        }

        return $this->_intro;
    }

    /**
     * setIntro
     * Sets the var with the given value
     *
     * @param Value Text
     */
    public function setIntro($value)
    {
        $this->_intro = $value;
    }

    /**
     * getMessage
     * Return the var from this object or the default
     * @return Message
     */
    public function getMessage()
    {
        if (empty($this->_message)) {
            return 'Geen Berichtje';
        }

        return $this->_message;
    }

    /**
     * setMessage
     * Sets the var with the given value
     *
     * @param Value Text
     */
    public function setMessage($value)
    {
        $this->_message = $value;
    }

    /**
     * getImageSource
     * Return the var from this object or the default
     * @return Image_source
     */
    public function getImageSource()
    {
        if (isset($this->_image_source)) {
            return $this->_image_source;
        }
    }

    /**
     * setImageSource
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setImageSource($value)
    {
        $this->_image_source = $value;
    }

    /**
     * getPublishDate
     * Return the var from this object or the default
     * @return Publish_date
     */
    public function getPublishDate()
    {
        if (isset($this->_publish_date)) {
            return $this->_publish_date;
        }
    }

    /**
     * setPublishDate
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setPublishDate($value)
    {
        $this->_publish_date = $value;
    }

    /**
     * getCreationDate
     * Return the var from this object or the default
     * @return Creation_date
     */
    public function getCreationDate()
    {
        if (isset($this->_creation_date)) {
            return $this->_creation_date;
        }
    }

    /**
     * setCreationDate
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setCreationDate($value)
    {
        $this->_creation_date = $value;
    }

    /**
     * getLastUpdated
     * Return the var from this object or the default
     * @return Last_updated
     */
    public function getLastUpdated()
    {
        if (isset($this->_last_updated)) {
            return $this->_last_updated;
        }
    }

    /**
     * setLastUpdated
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setLastUpdated($value)
    {
        $this->_last_updated = $value;
    }

    /**
     * getCategoryTitle
     * Return the var from this object or the default
     * @return Category_title
     */
    public function getCategoryTitle()
    {
        if (isset($this->_category_title)) {
            return $this->_category_title;
        }
    }

    /**
     * setCategoryTitle
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setCategoryTitle($value)
    {
        $this->_category_title = $value;
    }

    /**
     * getUserName
     * Return the var from this object or the default
     * @return User_name
     */
    public function getUserName()
    {
        if (isset($this->_user_name)) {
            return $this->_user_name;
        }
    }

    /**
     * setUserName
     * Sets the var with the given value
     *
     * @param Value String
     */
    public function setUserName($value)
    {
        $this->_user_name = $value;
    }

    /* ----- END   --- Getters and setters ----- */


}
 