<?php

namespace Library\Controllers;

use Library\Services\Database;

/**
 * Class BaseController
 *
 * Default controller
 */
class BaseController
{
    /*
     * Library\Services\DatabaseService
     */
    protected $_db_object;

    /**
     * Maak een nieuwe nieuwscontroller aan
     * en bewaar intern de database
     */
    public function __construct(Database $databaseServiceObject)
    {
        $this->_db_object = $databaseServiceObject;
    }


    /**
     * @return \Library\Services\Database
     */
    protected function getDbObject()
    {
        return $this->_db_object;

    }

}

