<?php

namespace Library\Controllers;

use Library\Models\NewsModel;
use Library\Services\View;

/**
 * Class newsController
 *
 * Mijn newsController die alle functionaliteit van de nieuwsberichten regelt
 * hier handel ik alle database dingen af
 * deze class geeft nieuwsbericht objecten terug
 * de weergave doe ik nu in een view.php
 */
class NewsController extends BaseController
{

    public function showNewsList()
    {
        $all_news = $this->getAllNewsModels();

        if (empty($all_news)) {
            return;
        }

        View::getView('News/NewsList', $all_news);
    }

    /**
     * Alle nieuwsberichten ophalen
     *
     * return alle nieuwsberichten uit de database
     * @return array
     */
    public function getAllNewsModels()
    {
        // voer hier de query uit om alle posts op te halen
        $query = "	SELECT p.*, c.title as category_title, concat(u.firstname, ' ', u.middlename, ' ', u.lastname ) as user_name
					FROM `posts` AS p
					INNER JOIN `categories` AS c ON c.id=p.category_id
					LEFT JOIN `users` AS u ON u.id=p.user_id
					WHERE p.`publish_date` IS NOT NULL 
					AND p.`publish_date` <= NOW()
					ORDER BY `publish_date`";
        /*
         * Zeg tegen mijn DatabaseServiceObject dat ik een query wil uitvoeren
         * Als het goed gaat wil ik ook een result terug
         */
        try {
            $result = $this->getDbObject()->getResult($query);

        } catch (\Exception $exception) {
            echo '<h1>Mislukt!</h1>';

            echo $exception->getMessage();
        }

        // maak een array aan die later terug kan geven
        $posts = [];

        // Loop door het mysql result heen en voeg steeds een auto toe aan de array
        while ($newsObject = $result->fetch_assoc()) {
            if (empty($newsObject['title'])) {
                continue;
            }

            $posts[] = new NewsModel($newsObject);
        }

        // geef alles terug
        return $posts;
    }

    /**
     * Show a news item
     *
     * @param $slug
     */
    public function showNewsItem($slug)
    {
        try {

            $news = $this->getNewsModel($slug);
            View::getView('News/News', $news);

        } catch (\Exception $exception) {
            echo '<h1>Mislukt!</h1>';

            echo $exception->getMessage();
        }
    }

    /**
     * Get one News Item
     *
     * @param $slug
     *
     * @return \Library\Models\NewsModel
     * @throws \Exception
     */
    public function getNewsModel($slug)
    {
        $query = "SELECT p.*, c.title as category_title, concat(u.firstname, ' ', u.middlename, ' ', u.lastname ) as user_name
			FROM `posts` AS p
			LEFT JOIN `categories` AS c ON c.id=p.category_id
			LEFT JOIN `users` AS u ON u.id=p.user_id
			WHERE post_url = ?";

        $result = $this->getDbObject()->getResult($query, 's', $slug);

        return new NewsModel($result->fetch_assoc());

    }


}

