<?php

namespace Library\Controllers;

use Library\Services\View;

class LoginController extends BaseController
{
    public function __construct(\Library\Services\Database $databaseServiceObject)
    {
        parent::__construct($databaseServiceObject);

        if ( ! session_id()) {
            session_start();
        }

        $this->loginOutAction();

        $this->loginAction();
    }

    /**
     * Logout function
     */
    private function loginOutAction()
    {
        if ( ! isset($_GET['logout']) || ! self::isLoggedIn()) {
            return;
        }

        unset($_SESSION['user']);
        unset($_SESSION['ingelogd']);

        header('location:' . ROOT_URL);

    }

    /**
     * Login function
     */
    private function loginAction()
    {
        if ( ! isset($_POST['name']) && ! isset($_POST['pass'])) {
            return;
        }
        $user = $this->getDbObject()->cleanString($_POST['name']);
        $pass = $this->hashPass($_POST['pass']);

        $query = 'SELECT id, username, firstname, middlename, lastname, gender, email FROM users WHERE username = ?  AND password=?';
        try {
            $user = $this->getDbObject()->getResult($query, 'ss', $user, $pass);
            $this->storeUser($user);
        } catch (\Exception $exception) {
            echo '<h1>Mislukt!</h1>';

            echo $exception->getMessage();
        }
    }

    /**
     * Ben ik ingelogd?
     *
     * @return bool
     */
    public static function isLoggedIn()
    {
        if (isset($_SESSION['ingelogd'])) {
            return true;
        }

        return false;
    }

    /**
     * @param $pass
     *
     * @return string
     */
    private function hashPass($pass)
    {
        $pass = $this->getDbObject()->cleanString($pass);

        return md5('mijnHASH' . $pass);
    }

    private function storeUser($result)
    {
        $user = $result->fetch_assoc();
        if ( ! isset($user['id'])) {
            return false;
        }
        $_SESSION['user']     = $user;
        $_SESSION['ingelogd'] = true;

    }

    /**
     * Formulier laten zien of niet
     */
    public static function showLoginForm()
    {
        if (self::isLoggedIn()) {
            echo '<div class="text-info navbar-form navbar-right">welkom: ' . self::getUserName() . ' <a href="?logout=true" class="btn btn-primary">Uitloggen</a></div>';

            return;
        }

        View::getView('Login/LoginForm');
    }

    /**
     * Wat is mijn naam?
     *
     * @return string
     */
    public static function getUserName()
    {
        if ( ! self::isLoggedIn()) {
            return '';
        }

        if ( ! isset($_SESSION['user']['username'])) {
            return '';
        }

        return $_SESSION['user']['username'];
    }
}

