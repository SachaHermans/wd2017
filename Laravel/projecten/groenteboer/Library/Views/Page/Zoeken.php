<div class="row">
    <div class="col-lg-4">
        <h2>Hier kun je zoeken</h2>
        <p>Als je hier een tekst in tikt dan moet het resultaat gaan verschijnen tijdens het tikken.</p>
        <form method="get">
            <div class="form-group">
                <input type="text" placeholder="zoeken maar..." name="s" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Zoeken</button>
        </form>
        <div class="result_holder">
            <h3>Hier zie het resultaat</h3>
            <ul class="zoek_resultaat list-group">
                <li class="list-group-item"><a href="mijn_url">Dit is een zoek resultaat</a></li>
                <li class="list-group-item"><a href="mijn_url">Dit is een zoek resultaat</a></li>
                <li class="list-group-item"><a href="mijn_url">Dit is een zoek resultaat</a></li>
                <li class="list-group-item"><a href="mijn_url">Dit is een zoek resultaat</a></li>
            </ul>
        </div>
    </div>
</div>
