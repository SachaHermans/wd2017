function loadAjax() {
	var xhttp                = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if ( this.readyState == 4 && this.status == 200 ) {
			document.getElementById( "content" ).innerHTML = this.responseText;
		}
	};
	xhttp.open( 'POST', 'ajax.php', true );
	xhttp.send();
}