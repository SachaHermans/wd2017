<?php

namespace Library;

use Library\Controllers\NewsController;
use Library\Services\View;
use Library\Settings\Config;

require_once('../Library/autoload.php');

/*
 * Laad de instellingen
 */
$config = new Config();

/*
 * Maak een nieuws controller object
 */
$newsController = new NewsController($config->getDatabase());

/*
 * Haal de juiste pagina binnen
 */
$mijnArr   = $config->getCurrentItem();

View::getView('Main/header');

/*
 * laat het overzicht zien als de url leeg is
 */

switch ($mijnArr['class']) {
    case 'home' :
    default:
        $newsController->showNewsList();
        break;
    case 'page':
        View::getViewWith404('Page/' . ucfirst($mijnArr['item']));
        break;
    case 'news':
        $newsController->showNewsItem($mijnArr['item']);
        break;
}


View::getView('Main/footer');
