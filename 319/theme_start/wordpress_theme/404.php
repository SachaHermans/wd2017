<?php get_header() ?>

  		<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">						
                
							<header>
                                <h2>Pagina bestaat niet...</h2>
                                <p>Helaas, de door u opgevraagde pagina bestaat niet.</p>
                            </header>  
        
        
      
                 
                 <div class="row 100%">
					<div class="6u 12u$(medium)">
                    
                    <p>Er is iets misgegaan. Wellicht heeft u een tikfout gemaakt in het webadres of de pagina bestaat niet. Geen probleem, klik elders in de website naar dat wat u zocht.</p>
                    
                    <p>De foutmelding Error 404 of Not Found (niet gevonden) is een HTTP-statuscode. Bij het fenomeen waarbij aangeklikte links niet meer werken, of typefouten zijn opgetreden, verschijnt deze foutmelding in het browserscherm.</p>
                    
                    </div>
                    <div class="6u 12u$(medium)">

<p>Misschien helpen deze links u verder:</p>

   <ul>
					<?php wp_list_pages('title_li=&sort_column=menu_order'); ?>
                    </ul>
                   
                    </div>
              	</div>

	    
    </div>
</section>
 

    
<?php get_footer() ?>





