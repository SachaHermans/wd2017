<?php get_header() ?>
<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">						
                
							<header>
                                <h2>Zoeken</h2>
                    <p>Resulaten met <?php the_search_query(); ?>'</p>
                            </header>  
        
        
      

	<?php if (have_posts()) : ?>
		

		<?php while ( have_posts() ) : the_post(); ?>
		
			<p><a href="<?php the_permalink() ?>" title="<?php the_title() ?>">&raquo; <?php the_title() ?></a></p>	
           
         			
			
		<?php endwhile; ?>
        
        	<?php get_search_form(); ?>

	
	<?php else : ?>

		
			<h3>Niets gevonden</h3>
			
			<p>Sorry, we hebben niets gevonden met deze invoer. Probeer het nog eens met wat andere termen.</p>
		
			<?php get_search_form(); ?>
		

	<?php endif; ?>
	


</div>
</section>

<?php get_footer() ?>