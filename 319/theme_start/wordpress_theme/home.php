<?php get_header() ?>
	
    		<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">		
					<header>
						<h1>Nieuws</h1>
						<p>Blijf op de hoogte</p>
					</header>
                    
                      <div class="row">
                      <?php if ( have_posts() ) {
							while ( have_posts() ) {
								the_post(); ?>	                                
                              
                                    <div class="4u 12u$(small)">                                        
                                            <h3><a style="color:#da0000" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                           	<?php if ( has_post_thumbnail() ) { ?>
                                            	<span class="image fit"><a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_title(); ?>" /></a></span>
                                        	<?php } ?>    
                                            <p><?php the_excerpt(); ?></p>   
                                            
                                            <ul class="actions">
                                            <li><a href="<?php the_permalink(); ?>" class="button fit icon fa-arrow-circle-right">Lees verder</a></li>
                                            </ul>                          
                                    	
                                    </div>
                       <?php }} ?> 
                       </div>             
  				                
				</div>
			</section>                         
                   
<?php get_footer() ?>