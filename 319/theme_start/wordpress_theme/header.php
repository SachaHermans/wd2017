<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />      
       
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->	
		<!--[if lte IE 8]><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/ie9.css" /><![endif]-->
        
        <?php
		if ( is_admin_bar_showing() ) { ?>
			<style>
			#header {
				top:32px!important;
				}			
			</style>
		<?php }
		?>              
	
	<?php wp_head() ?>        
	</head>
	<body id="top" <?php body_class(); ?>> 
	
    <!-- Header -->
			<header id="header" >

				<!-- Logo -->
					<div class="logo">
						<a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
					</div>

				<!-- Nav -->
					<nav id="nav">
						<?php	$defaults = array(
								'theme_location'  => 'primary',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'localisation',
								'menu_id'         => 'localisation',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',						
								
							);							
							wp_nav_menu( $defaults );							
							?>	
					</nav>
			</header>
      