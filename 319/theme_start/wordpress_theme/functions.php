<?php

/**
 * Dump
 */

function dump($stuff) {
	
	echo '<pre>';
	var_dump($stuff);
	echo '</pre>';
		
	}
	
/**
 * Proper way to enqueue scripts and styles
 */
function add_theme_styles() {
	
	// styles
	wp_enqueue_style( 'main', get_stylesheet_uri());
	wp_enqueue_style( 'linden', get_template_directory_uri() . '/assets/css/linden.css');
		
	// scripts		
	wp_enqueue_script( 'dropotron', get_template_directory_uri() . '/assets/js/jquery.dropotron.min.js', array('jquery'),true);
	wp_enqueue_script( 'scrollex', get_template_directory_uri() . '/assets/js/jquery.scrollex.min.js', array('jquery'),true);
	wp_enqueue_script( 'scrolly', get_template_directory_uri() . '/assets/js/jquery.scrolly.min.js', array('jquery'),true);
    wp_enqueue_script( 'selectorr', get_template_directory_uri() . '/assets/js/jquery.selectorr.min.js', array('jquery'),true);
	wp_enqueue_script( 'skel', get_template_directory_uri() . '/assets/js/skel.min.js', array('jquery'),true);
	wp_enqueue_script( 'util', get_template_directory_uri() . '/assets/js/util.js', array('jquery'),true);
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'),true);	
	
}

add_action( 'wp_enqueue_scripts', 'add_theme_styles' );


/**
 * Theme Support
 */
register_nav_menus( array(
    'primary' => __( 'Hoofdmenu', 'localisation' ),
	'secundairy' => __( 'Footermenu', 'localisation' )
) );


// Post thumbnail
add_theme_support( 'post-thumbnails' );


// Editor CSS
function theme_add_editor_styles() {
    add_editor_style( '/assets/css/style-admin.css' );
}
add_action( 'admin_init', 'theme_add_editor_styles' );