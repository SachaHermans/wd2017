<?php get_header() ?>
<?php the_post() ?>




<!-- Main -->
		<section id="main" class="wrapper">
            <div class="inner">

					<header>
						<h1><?php the_title() ?></h1>
						<p>{subtitle}</p>
					</header>
				
                
                    <span class="image fit"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></span>
                    
                    <?php the_content() ?>
                

				</div>
			</section>
<?php get_footer() ?>