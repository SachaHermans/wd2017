<?php get_header() ?>
<?php the_post() ?>

<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<span class="icon fa-laptop"></span>
					<h1><?php the_title(); ?></h1>
					<ul class="actions">
						<li><a href="#one" class="button wide scrolly">Get Started</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="wrapper">
				<div class="inner">
					<div class="split style1 centered reversed">
						<section>
							<span class="image fit portrait"><img src="<?php bloginfo('template_url'); ?>/images/pic02.jpg" alt="" /></span>
						</section>
						<section>
							<h2>Adipiscing ornare risus morbi est est blandi magna vel euismod tempus</h2>
							<div class="features">
								<article class="icon fa-diamond">
									<h3>Sed veroeros</h3>
									<p>Lorem ipsum dolor sit amet nulla etiam feugiat sed adipiscing.</p>
								</article>
								<article class="icon fa-file-o">
									<h3>Magna etiam</h3>
									<p>Lorem ipsum dolor sit amet nulla etiam feugiat sed adipiscing.</p>
								</article>
								<article class="icon fa-clone">
									<h3>Nulla consequat</h3>
									<p>Lorem ipsum dolor sit amet nulla etiam feugiat sed adipiscing.</p>
								</article>
								<article class="icon fa-envelope-o">
									<h3>Euismod morbi</h3>
									<p>Lorem ipsum dolor sit amet nulla etiam feugiat sed adipiscing.</p>
								</article>
							</div>
						</section>
					</div>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style1 special">
				<div class="inner">
					<div class="stats">
						<article>
							<h3><span>128</span> Sed magna</h3>
							<p>Lorem ipsum dolor nulla feugiat adipiscing.</p>
						</article>
						<article>
							<h3><span>640</span> Etiam dolor</h3>
							<p>Lorem ipsum dolor nulla feugiat adipiscing.</p>
						</article>
						<article>
							<h3><span>256</span> Nullam amet</h3>
							<p>Lorem ipsum dolor nulla feugiat adipiscing.</p>
						</article>
						<article>
							<h3><span>768</span> Sit euismod</h3>
							<p>Lorem ipsum dolor nulla feugiat adipiscing.</p>
						</article>
					</div>
				</div>
			</section>

		<!-- Three -->
			<section id="three" class="wrapper">
				<div class="inner">
					<ul class="tabs">
						
						<?php
						
						$args = array(
							'post_type' => 'post',
							'posts_per_page'=> 4,
							'order_by' => 'date',
							'order' => 'DESC',
							'cat' => 3
 						);
						
						$homeposts = new WP_Query($args);
						
						if( $homeposts->have_posts() ) : 
							//start loop
							while ( $homeposts->have_posts() ):
										$homeposts->the_post(); ?>
								
								<li>
								<h3><?php the_field('subtitle'); ?></h3>
									<div class="split reversed">
										<div class="content">
											<h2><?php the_title(); ?></h2>
											<p><?php the_excerpt(); ?></p>
											<?php the_category(); ?>
											<ul class="actions">
												<li><a href="<?php the_permalink(); ?>" class="button">Learn More</a></li>
											</ul>
										</div>
										<?php if (has_post_thumbnail() ) : ?>
										<div class="image"><img src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_title(); ?>" /></div>
										<?php endif; ?>
									</div>
								</li>
							<? endwhile;
						endif;
						
						wp_reset_postdata();
						
						?>
						
						
						
						
					</ul>
				</div>
			</section>

		<!-- Four -->
			<section id="four" class="quotes">
				<article class="icon fa-quote-left">
					<span class="image"><img src="<?php bloginfo('template_url'); ?>/images/pic03.jpg" alt="" data-position="center" /></span>
					<h2>"Sed ultrices consequat dolor nulla fringilla dignissim"</h2>
					<div class="author">
						<span class="name">Jane Doe</span>
						<span class="title">Ipsum dolor nullam</span>
					</div>
				</article>
				<article class="icon fa-quote-left">
					<span class="image"><img src="<?php bloginfo('template_url'); ?>/images/pic04.jpg" alt="" data-position="left" /></span>
					<h2>"Nam velit et lorem porta quis at pulvinar tellus nibh"</h2>
					<div class="author">
						<span class="name">John Doe</span>
						<span class="title">Ipsum dolor nullam</span>
					</div>
				</article>
			</section>

		<!-- Five -->
			<section id="five" class="wrapper">
				<div class="inner">
					<div class="split">
						<section>
							<h2>Feugiat lorem ipsum dolor velit amet dolor dignissim pharetra</h2>
							<span class="image fit"><img src="<?php bloginfo('template_url'); ?>/images/pic06.jpg" alt="" /></span>
							<p>Maecenas id feugiat nunc. Integer gravida augue libero, quis pellentesque amet pharetra a. Ut sagittis ipsum nec velit porttitor, sed convallis ligula pellentesque. Mauris et dignissim sem lacinia lorem ipsum dolor.</p>
							<ul class="actions">
								<li><a href="#" class="button">Learn More</a></li>
							</ul>
						</section>
						<section>
							<div class="ratings">
								<article>
									<h3>Ipsum amet</h3>
									<div class="progress" data-progress="60">60%</div>
								</article>
								<article>
									<h3>Adipiscing feugiat</h3>
									<div class="progress" data-progress="90">90%</div>
								</article>
								<article>
									<h3>Dolor ligula</h3>
									<div class="progress" data-progress="75">75%</div>
								</article>
								<article>
									<h3>Libero magna</h3>
									<div class="progress" data-progress="45">45%</div>
								</article>
								<article>
									<h3>Nec lacinia</h3>
									<div class="progress" data-progress="55">55%</div>
								</article>
							</div>
						</section>
					</div>
				</div>
			</section>

<?php get_footer() ?>