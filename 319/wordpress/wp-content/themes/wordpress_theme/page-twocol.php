<?php // Template Name: Twocol ?>
<?php get_header() ?>
<?php the_post() ?>




<!-- Main -->
		<section id="main" class="wrapper">
            <div class="inner">

					<header>
						<h1><?php the_title() ?></h1>
						<p><?php echo get_post_meta(get_the_ID(), 'Subtitel', true); ?></p>
					</header>
					
					<div class="row">
						<div class="6u 12u(small)">
						<?php the_content() ?>
						</div>
				
                		<div class="6u 12u(small)">
                		<?php if (has_post_thumbnail()) : ?>
                    		<span class="image fit"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></span>
                    	<?php endif; ?>
                    	</div>
                    	
                    
                    
                	</div>

				</div>
			</section>
<?php get_footer() ?>