<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp319');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0]`9TB4,W9 - s!w)Ii}oEjH`6E:)Ze>ribY1th&cnwpMcnYpt9-Sy5gz6~5_$Z:');
define('SECURE_AUTH_KEY',  ']3O6u%gnZTn nrx=nBQsf|D?-Y^_nyi<99kXN|O>s[w<3Z<]$?CYtJa3b/;OYECb');
define('LOGGED_IN_KEY',    '_{ubX$?W1N?B&RO2^Ku|EPcaM$H`A*VV4x9:3rf5&f~7Z3T}Rc^STy[`/ e;P[ml');
define('NONCE_KEY',        'v*;k,)J o:Cn3s-*ovtL,EAG>Dn=_,!IZ.p]S_(b8Q!&5-k3_*C~oqgs+B3C4bdL');
define('AUTH_SALT',        'h b%*J:sN1qw+[;^ge7t]n;pIYKlEFGGd{X)Tx;!N?Ij|!,DMR?(y.w|HU6Lvg}B');
define('SECURE_AUTH_SALT', 'cu[?MuK$kxY6~7X+?CjY~R1Tm#oP>>/^]YYk:cDO3Ml2eV{5/f+iqY?_ld@:HN*x');
define('LOGGED_IN_SALT',   '5[]L8K#f1;&r<kkJW|/(U:!T3b|$P1,|?P%1Hu)1H=CNCn{RFzO!H)uh2;**#iTy');
define('NONCE_SALT',       'N8s[H#6/ppb5h*0{~@rl8#n@;3B|R}1m>()gS]2b>8qua&.Sr::S(t]7YD~+3TV=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wd319_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
