# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21-0ubuntu0.16.04.1)
# Database: cmmproject
# Generation Time: 2018-03-03 16:02:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table migrations
# ------------------------------------------------------------

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(20,'2014_10_12_000000_create_users_table',1),
	(21,'2014_10_12_100000_create_password_resets_table',1),
	(22,'2018_02_05_133756_create_news_table',1),
	(23,'2018_02_07_115607_create_pages_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table news
# ------------------------------------------------------------

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `user_id`, `title`, `slug`, `body`, `publish_date`, `created_at`, `updated_at`)
VALUES
	(1,0,'Titel','titel','Duis sit amet dignissim ligula. Maecenas posuere vulputate gravida. Duis rhoncus sed arcu vel suscipit. Nam bibendum ante arcu, id cursus justo lacinia ut. Duis vestibulum massa sit amet tortor lobortis aliquam. Morbi in velit eget tellus rutrum semper. Etiam sodales semper neque, at porta sem. Fusce sit amet rutrum diam, ac ultricies sem.','2018-02-04','2018-02-04 13:00:25','2018-02-04 13:00:25'),
	(2,0,'Morbi scelerisque finibus nisl','morbi','Morbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\n\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\n\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-02-05','2018-02-05 13:49:41','2018-02-05 13:49:41'),
	(3,0,'TITEL','gzdf','Lekker puh!','2018-02-17','2018-02-05 13:49:57','2018-02-24 14:40:19'),
	(4,0,'Duis sit amet dignissim ligula','duis','Duis sit amet dignissim ligula. Maecenas posuere vulputate gravida. Duis rhoncus sed arcu vel suscipit. Nam bibendum ante arcu, id cursus justo lacinia ut. Duis vestibulum massa sit amet tortor lobortis aliquam. Morbi in velit eget tellus rutrum semper. Etiam sodales semper neque, at porta sem. Fusce sit amet rutrum diam, ac ultricies sem.\n\nMorbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\n\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\n\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-01-15','2018-02-05 13:49:57','2018-02-05 13:49:57'),
	(8,0,'Titel 3','titel3','Mijn derde bericht aangepast','2018-02-15','2018-02-17 15:29:05','2018-02-17 15:29:05');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `title`, `url`, `body`, `active`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'Over ons','over_ons','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat tortor a dignissim bibendum. Proin facilisis a est quis suscipit. Ut bibendum vehicula facilisis. Aliquam mauris odio, feugiat et magna eu, convallis tincidunt enim. Vestibulum eu euismod lectus, nec pharetra nisi.',1,0,'2018-02-07 12:13:39','2018-02-07 12:13:39'),
	(2,'Contact','contact','Dit is de contact pagina. Als je contact met ons wilt opnemen dan moet je even mailen naar info@cmm.nl',0,0,'2018-02-07 12:16:49','2018-02-07 12:16:54'),
	(3,'Welkom','welcome','Phasellus placerat tortor a dignissim bibendum. Proin facilisis a est quis suscipit. Ut bibendum vehicula facilisis. Aliquam mauris odio, feugiat et magna eu, convallis tincidunt enim. Vestibulum eu euismod lectus, nec pharetra nisi.',1,0,'2018-02-07 12:56:09','2018-02-07 12:56:13');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Sacha','sacha@cmm.nl','$2y$10$BULzIubQhtZ9UtK4A6HXguCVbr3.YFpcC1g8avP5ivYGGDxVzF8AC','eFRcKaGxBtyGseXYmi1q8uBj10N0l59NfnaoPFEifGDHgAIDgbwSP2Ga9qeT','2018-02-24 10:32:26','2018-02-24 10:32:26');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
