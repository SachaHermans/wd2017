@extends( 'layout.master' )

@section('title')
{{ $news->title }}
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>{{ $news->title }}</h1>
            
            @auth
            <form action="/news/{{$news->id}}" method="post" onsubmit="return confirm('Wil je dit bericht verwijderen?')">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="delete">
            	<button class="btn btn-danger" type="submit">Delete</button>
            </form>
            
            <a href="/news/{{ $news->id }}/edit" class="btn btn-primary">Edit</a>
            @endauth
            
            <span>{{ $news->publish_date }}</span>
            <p>
                {{ $news->body }}
            </p>
            <a href="/news">Nieuws overzicht</a>
        </div>
    </div>
@endsection