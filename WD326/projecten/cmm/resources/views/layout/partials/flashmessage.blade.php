@if($flash_message = session('message'))
<div class="alert alert-success">
	{{ $flash_message }}
</div>
@endif