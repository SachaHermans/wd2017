<?php

/**
 * Autoloader
 *
 * This class add the autoload functionality to the project
 */
class autoload
{
    static public function _autoload($class)
    {
        $path = str_replace('\\', '/', $class);

        if (file_exists(self::trailingslashit(dirname(__DIR__)) . $path . '.php')) {
            require_once(self::trailingslashit(dirname(__DIR__)) . $path . '.php');
        }
    }

    /**
     * Appends a trailing slash.
     *
     * @param $string
     *
     * @return string
     */
    public static function trailingslashit($string)
    {
        return self::untrailingslashit($string) . '/';
    }

    /**
     * Removes trailing forward slashes and backslashes if they exist.
     *
     * @param $string
     *
     * @return string
     */
    public static function untrailingslashit($string)
    {
        return rtrim($string, '/\\');
    }
}

spl_autoload_register('autoload::_autoload');