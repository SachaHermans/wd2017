<?php

namespace Library\Settings;

use Library\Controllers\LoginController;
use Library\Services\Database;

class Config
{
    /*
     * Library\Services\DatabaseService
     */
    protected $_databaseSetting;


    public function __construct()
    {
        $this->activateSession();
        $this->getSettings();
        $this->setDebugLevel();

        new LoginController($this->getDatabase());

    }

    /**
     * Zorg ervoor dat er maximaal 1 keer is gestart
     */
    public function activateSession()
    {
        if (session_id()) {
            return;
        }
        session_start();
    }

    /**
     * Zorg dat de juiste intsellingen beschikbaar zijn
     */
    public function getSettings()
    {
        switch ($_SERVER['HTTP_HOST']) {
            case 'groenteboer.test' :
                define('ROOT_URL', 'http://groenteboer.test/');
                define('DOCUMENT_ROOT', '/home/vagrant/code/groenteboer/');
                define('LIBRARY_ROOT', DOCUMENT_ROOT . 'Library/');
                define('PUBLIC_ROOT', DOCUMENT_ROOT . 'public_html/');

                define('DB_USER', 'homestead');
                define('DB_PASS', 'secret');
                define('DB_HOST', 'localhost');
                define('DB_NAME', 'groenteboer');
                define('DEBUG_VIEWS', true);
                define('DEBUG', true);

                break;

            default:
                define('ROOT_URL', 'http://groenteboer.nl/');
                define('DOCUMENT_ROOT', '/boer/domains/groenteboer.nl/');
                define('LIBRARY_ROOT', DOCUMENT_ROOT . 'Library/');
                define('PUBLIC_ROOT', DOCUMENT_ROOT . 'public_html/');

                define('DB_USER', 'root');
                define('DB_PASS', 'root');
                define('DB_HOST', 'localhost');
                define('DB_NAME', 'dbgboer');
                define('DEBUG_VIEWS', false);
                define('DEBUG', true);
                break;

        }
    }

    /**
     * Set the debug level
     */
    public function setDebugLevel()
    {
        if ( ! defined(DEBUG) || ! DEBUG) {
            return;
        }

        ini_set('error_reporting', 1);
        error_reporting(E_ALL);
    }

    /**
     * @return \Library\Services\Database
     */
    public function getDatabase()
    {
        if ( ! empty($this->_databaseSetting)) {
            return $this->_databaseSetting;
        }

        $this->_databaseSetting = new Database();

        return $this->_databaseSetting;
    }

    /**
     * @return array
     */
    public function getCurrentItem()
    {
        $class = $url = 'home';

        /*
         * haal de titel uit de url als die gegeven is
         */
        $mijnArr = preg_split("/\//", $_SERVER['REQUEST_URI'], 0, PREG_SPLIT_NO_EMPTY);

        /*
         * De naam van een bericht zit in het laatste stukje van de url
         */
        if (count($mijnArr) > 0) {
            $class = 'page';
            $url   = end($mijnArr);
        }
        if (count($mijnArr) > 1) {
            $class = $mijnArr[0];
        }

        define('PAGE_TITLE', $url);

        return [
            'class' => $class,
            'item'  => $url,
        ];

    }
}







