<div class="row">
    <?php
    foreach ($data as $news_model) {
        echo '<div class="col-lg-4">';
        echo '<h2>' . $news_model->getTitle() . '</h2>';
        echo $news_model->getIntro();
        echo '<a href="\news\\' . $news_model->getPostUrl() . '">Lees meer</a>';
        echo '</div>';
    }
    ?>
</div>