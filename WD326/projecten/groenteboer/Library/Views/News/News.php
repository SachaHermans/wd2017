<div class="row">
    <div class="col-lg-4">
        <h2><?= $data->getTitle() ?></h2>
        <h3><?= $data->getIntro() ?></h3>
        <p><?= $data->getMessage() ?></p>
    </div>
</div>
