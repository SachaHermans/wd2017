
        </div>
        <hr>

        <footer>
            <p>&copy; <?=date('Y')?> College of MultiMedia</p>
        </footer>
    </div>
    <!-- /container -->
    <script src="<?php echo ROOT_URL; ?>js/jquery.js"></script>
    <script src="<?php echo ROOT_URL; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo ROOT_URL; ?>js/groenteboer.js"></script>
    <script src="<?php echo ROOT_URL; ?>js/keydown.js"></script>
</body>
</html>