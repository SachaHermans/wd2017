<?php
use Library\Services\View;

/*
 * zorg ervoor dat er altijd een titel is
 */
$page_title = 'default titel';
if (defined('PAGE_TITLE')) {
    $page_title = PAGE_TITLE;
}
?><!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>College of MultiMedia | <?php echo $page_title; ?></title>

    <link href="<?php echo ROOT_URL; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo ROOT_URL; ?>css/main.css" rel="stylesheet">
</head>

<body>
<header>
    <?php View::getView('Main/menu'); ?>
</header>

<div class="container">
