<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= ROOT_URL ?>">Groenteboer</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= ROOT_URL ?>">Home</a></li>
                <li><a href="<?= ROOT_URL; ?>about">About</a></li>
                <li><a href="<?= ROOT_URL; ?>contact">Contact</a></li>
                <li><a href="<?= ROOT_URL; ?>zoeken">Zoeken</a></li>
            </ul>
            <?php
            # Login Class laden
            \Library\Controllers\LoginController::showLoginForm();
            ?>
        </div>
    </div>
</div>
    