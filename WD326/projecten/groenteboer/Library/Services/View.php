<?php

namespace Library\Services;


class View
{
    /**
     * @param        $view
     * @param string $data
     *
     * @return string
     */
    public static function getView($view, $data = '')
    {
        $document = LIBRARY_ROOT . 'Views/' . $view . '.php';

        if (defined(DEBUG_VIEWS) && DEBUG_VIEWS) {
            echo '<div class="debug">getView: ' . $document . '</div>';
        }

        if ( ! file_exists($document)) {
            return false;
        }

        include($document);
            return true;
    }

    /**
     * @param        $view
     * @param string $data
     *
     * @return string
     */
    public static function getViewWith404($view, $data = '')
    {
        if ( false === self::getView($view, $data) ) {
            self::getView('Page/404', $data);
        }

    }
}

