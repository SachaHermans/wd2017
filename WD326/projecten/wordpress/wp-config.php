<?php

// BEGIN iThemes Security - Deze regel niet wijzigen of verwijderen
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Deactiveer bestandsbewerker - Beveiliging > Instellingen > WordPress tweaks > Bestandsbewerker
// END iThemes Security - Deze regel niet wijzigen of verwijderen

/**
 * Default config for the website
 */
switch ( $_SERVER['HTTP_HOST'] ) {
	case 'wordpress.cmm.test':
		define( 'DB_NAME',      'wordpress_cmm' );
		define( 'DB_USER',      'homestead' );
		define( 'DB_PASSWORD',  'secret' );
		define( 'DB_HOST',      'localhost' );
		define( 'DB_CHARSET',   'utf8' );
		define( 'DB_COLLATE',   '' );

		define( 'WP_CONTENT_DIR',   dirname( __FILE__ ) . '/public_html/content' );
		define( 'WP_CONTENT_URL',   '/content' );
		// define( 'WP_TEMP_DIR',      WP_CONTENT_DIR . '/uploads/tmp' );
		$table_prefix = 'cmm_';

		define( 'WP_DEBUG',             true );
		define( 'WP_DEBUG_DISPLAY',     false );
		define( 'WP_DEBUG_LOG',         false );
		ini_set( 'display_errors',      1 );
		break;

	case 'webshopcmm.test':
	default:
		define( 'DB_NAME',      'webshop_cmm' );
		define( 'DB_USER',      'homestead' );
		define( 'DB_PASSWORD',  'secret' );
		define( 'DB_HOST',      'localhost' );
		define( 'DB_CHARSET',   'utf8' );
		define( 'DB_COLLATE',   '' );

		define( 'WP_CONTENT_DIR',   dirname( __FILE__ ) . '/public_html/content' );
		define( 'WP_CONTENT_URL',   '/content' );
		#define( 'WP_TEMP_DIR',      WP_CONTENT_DIR . '/uploads' );
		$table_prefix = 'cmm_';

		define( 'WP_DEBUG',             true );
		define( 'WP_DEBUG_DISPLAY',     true );
		define( 'WP_DEBUG_LOG',         true );
		break;
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key
 * service} You can change these at any point in time to invalidate all existing cookies. This will force all users to
 * have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?cE.wZ 8Q#&$.H@oaf%t`U VBzYV|YfuCEv)LRD-{;A(w,uD@fZ|){_y;Zq;AIg1');
define('SECURE_AUTH_KEY',  'f%i!>t[}YfMaI0X)D.~RDg}pO@h&S1~N8z.:stAy^MD<V6nYq~/R#163.rnPTCmS');
define('LOGGED_IN_KEY',    've>rM~Oz6y<6?|t&{]a:QIsQaG>xy>/)Dc=V/&8}/=%h,S+uRc /LaJlY#lPvy63');
define('NONCE_KEY',        ':^MC0kxz4.r(dcrdk=OU5K2v+R_|oN0S4`Kg/lMYAp8vER!9nnp,Ph:~LnqAQQ,A');
define('AUTH_SALT',        '0m|nA($0mORC3PR_hHi` NIO+dv];u-aSX|5/o*:<bot%&yXR72@3e?$><l#VR7)');
define('SECURE_AUTH_SALT', 'L^},k}`@@H&lPP{7-~*T`GC)ZN.;EU+p.w-0,9OT}[)+J~^l*@W@U$t3cboz1H#O');
define('LOGGED_IN_SALT',   'xA8d*hx65apGr%+zS4G}0cA$h^+[f[qu6/s JM-qTwW]uSwG@Z+~L9Z(^(G-RlJC');
define('NONCE_SALT',       ')a-!qRwQ$KFTMt` zo&IG)@M;[XO}]T?[?5yJiNGuovI}TQRF&diq5f~g^>>tQ9B');

/**#@-*/

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define( 'WPLANG', 'nl_NL' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}


/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );