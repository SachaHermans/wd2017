<?php
require_once '../includes/settings.php';
require_once '../includes/functions.php';

$page_active = basename($_SERVER["PHP_SELF"], ".php");

include_once '../includes/header.php';

//$title="College of MultiMedia | PHP MySQL";
?>


	<div class="jumbotron">
        <div class="container">
			<img src="../images/bannergt.jpg" class="img-responsive" alt="Gran Turismo Sport" width="590" height="251" title="Gran Turismo Sport">
		</div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2>Introductie van de Honda Sports Vision Gran Turismo: een fusie van dynamiek en functionaliteit</h2>

                <p>Honda gaat tijdens de zoektocht naar nieuwe waarden in auto's altijd uit van de mens en daarop vormt de Honda Sports Vision Gran Turismo, in samenwerking met Gran Turismo ontwikkeld, geen uitzondering. Er is nu een filmpje beschikbaar waarin je de nieuwe sportauto in al zijn glorie kunt bewonderen.</p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Lewis Hamilton, de maestro van Gran Turismo Sport, pakt vierde F1-titel</h2>

                <p> Op 12 oktober werd F1-coureur Lewis Hamilton de maestro van Gran Turismo Sport. Op 30 oktober pakte hij hij na afloop van de grand prix van Mexico, de achttiende race van het seizoen, zijn vierde F1-titel. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>IsoRivolta Zagato Vision Gran Turismo onthuld tijdens de Tokyo Motor Show</h2>

                <p>Het Italiaanse automerk Iso, opgericht door Renzo Rivolta, maakte in de jaren 60 en 70 aantrekkelijke sportauto's. Op 25 oktober maakte tijdens de Tokyo Motor Show, op de stand van Gran Turismo, een nieuwe telg van deze familie zijn debuut voor de camera's van de pers: de IsoRivolta Zagato Vision Gran Turismo.</p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
        </div>
<?php
		include_once '../includes/footer.php';