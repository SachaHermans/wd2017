Ik heb je settings even aangepast zodat ik ook lokaal kan werken.

Maak gebruik van een url en path constante, dan weet je zeker dat het path naar elk document klopt.

Dit hoer je maar op 1 plek te doen als je gebruik maakt van je settings.php
// laat alle errors zien
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

Bij if statements moet je even opletten dat je altijd gebruik maakt van {}
En Yoda stijl en je quotes.
Dit:

	if (($inlog_naam!=="") || ($inlog_ww!==""))
		$_SESSION['ingelogd']=true;
Moet eigenlijk zo:
	if ( '' !== $inlog_naam || '' !== $inlog_ww ) {
		$_SESSION['ingelogd']=true;
	}
Hoewel deze controle eigenlijk niet klopt omdat de naam of het wachtwoord nu ingevuld moet zijn en je niet controleer op wat er in de sessie staat.

Bij login.php op regel 9 staat dit:
$_SESSION['ingelogd']=false;

Je bent dus altijd uitgelogd

Hier moet je geen gebruik maken van ronde haken:
$inlog_naam=($_POST["inlog_naam"]);

Je doet een controle op $_POST['inlog_naam']
Maar die verstuur je nooit, ik zie nergens een veld met deze name langs komen

Denk aan je quote gebruik, niet zoveel dubbele quotes gebruiken.

Probeer niet een value op de placeholder plek te gebruiken.