<?php
session_start();
//laden van instellingen en functies
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/settings.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php';

$page_active = basename($_SERVER["PHP_SELF"], ".php");

include_once ROOT_PATH . 'includes/header.php';

//$title="College of MultiMedia | PHP MySQL";
?>    
    <div class="jumbotron">
        <div class="container">
            <?php   $h1="hello, world!";
                    echo "<h1>" . ucfirst($h1) . "</h1>";
            ?>

            <p>Dit is een template voor een eenvoudige website. <br>
				Het omvat een grote callout genoemd de heldenEenheid en drie ondersteunende stukken of inhoud. Gebruik het as een uitgangspunt om iets</p>

            <p><a href="pages/about.php" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
              </div>
            <div class="col-lg-4">
                <?php
                    $titel="Het fluitketeltje";
                    $omschrijving='Meneer is niet thuis en mevrouw is niet thuis,<br>het keteltje staat op het kolenfornuis,<br>de hele familie is uit,<br>en het fluit en het fluit en het fluit: túúúút';
                    $date=date('l j F Y');
                    toon_bericht($titel, $omschrijving, $date);
                ?>
                <p><a class="btn btn-default" href="pages/bericht.php">View details &raquo;</a></p>
            </div>
            
        </div>
<?php
		include_once ROOT_PATH . 'includes/footer.php';