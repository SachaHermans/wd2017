# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.35)
# Database: cmm_wd312_nieuwsberichten
# Generation Time: 2018-02-25 10:51:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table nieuwsberichten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nieuwsberichten`;

CREATE TABLE `nieuwsberichten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `publicatie_datum` datetime DEFAULT NULL,
  `update_datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` text,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schrijver_id` (`user_id`),
  CONSTRAINT `schrijver_id` FOREIGN KEY (`user_id`) REFERENCES `schrijvers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `nieuwsberichten` WRITE;
/*!40000 ALTER TABLE `nieuwsberichten` DISABLE KEYS */;

INSERT INTO `nieuwsberichten` (`id`, `titel`, `user_id`, `publicatie_datum`, `update_datum`, `content`, `url`)
VALUES
	(1,'Laatste beroepsvisser Amsterdam vindt levende zeehond in netten',1,'2017-11-27 13:31:03','2017-11-26 13:31:08','Al jarenlang doet visser Piet Ruijter in opdracht van het havenbedrijf Amsterdam onderzoek naar de visstanden in de haven. Met kleine netten en fuiken bekijkt hij wekelijks wat er in het Amsterdamse water rondzwemt. Gisteren bleek dat een heuse zeehond te zijn.\nToen Ruijter, de laatste beroepsvisser van Amsterdam, gisteren zijn netten ophaalde in de Amerikahaven, vond hij daarin zeebaarzen, gewone baarzen, grote Chinese wolhandkrabben, snoekbaarzen, tongen, wijting, steenbolk en een bot. Maar er bleek ook een zeehond met zijn staart vast te zitten. Ruijter bevrijdde het beestje meteen. De zeehond keek nog even om, vertelt Ruijter aan AT5. Maar dook daarna de diepte in.\nLees ook: Zeehond gespot in Westelijk Havengebied\nEnkeltje Amsterdam\nHet is niet de eerste keer dat er een zeehond in Amsterdamse wateren zwemt, maar het is wel heel bijzonder, vertelt stadsecoloog Martin Melchers. De sluizen bij IJmuiden zijn de toegangspoort voor de zeehonden. Bij het manoeuvreren van de schepen vanuit zee, voor en in de sluizen, stroomt schroefwater voor en achterwaarts. Hierdoor raken veel vissen uit balans en zijn een makkelijke prooi voor zeehonden. Als de sluisdeur achter de zeehond dichtgaat is de enige richting een enkeltje Amsterdam.\nIn 2011 zwierf er zon zeven weken lang een zeehond door de stad. Het beestje werd zelfs in de Amstel gezien.','laatste-beroepsvisser-amsterdam-vindt-levende-zeehond-in-netten'),
	(2,'WD312',2,'2017-11-26 13:39:50','2017-11-26 13:39:50','Praesent commodo, arcu quis eleifend consequat, felis lorem posuere justo, eget gravida leo enim at lacus. Sed molestie malesuada nisi et pulvinar. Sed ut tincidunt ipsum. Integer ac volutpat sem. Praesent eget convallis sem, eget placerat erat. Curabitur tincidunt maximus nisl quis tristique. Pellentesque vitae maximus quam. In mollis libero eu nulla fringilla, nec egestas odio pulvinar. Morbi venenatis lorem at facilisis pulvinar. Nullam sollicitudin tincidunt luctus. Vestibulum id est interdum, suscipit sapien id, congue odio.','WD312'),
	(3,'Vandaag is het Zaterdag',2,'2017-11-26 13:39:51','2017-11-26 13:39:51','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus convallis egestas ornare. Praesent imperdiet metus non cursus suscipit. Cras sit amet mi mollis, facilisis nibh in, tempus nisi. Duis eget urna dapibus sem rutrum viverra. Sed tellus diam, finibus vel libero nec, vulputate viverra erat. Nunc eget sodales metus. Phasellus vitae placerat magna, eu congue nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue augue ac lacus ultricies egestas. Morbi vel sollicitudin augue, et fringilla nunc.','zaterdag'),
	(4,'Hallo allemaal',3,'2017-11-26 13:39:52','2017-11-26 13:39:52','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.','hoi'),
	(28,'Hallo allemaal',NULL,NULL,'2018-02-25 11:24:05','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.',NULL),
	(29,'Dag Ernie',NULL,NULL,'2018-02-25 11:25:56','Wat ben ik blij dat ie het eindelijk doet!',NULL),
	(30,'Dag Ernie',NULL,NULL,'2018-02-25 11:39:35','Wat ben ik blij dat ie het eindelijk doet!',NULL),
	(31,'Dag Ernie',NULL,NULL,'2018-02-25 11:41:42','Wat ben ik blij dat ie het eindelijk doet!',NULL),
	(32,'Dag Ernie',NULL,NULL,'2018-02-25 11:42:40','Wat ben ik blij dat ie het eindelijk doet!',NULL),
	(33,'Hallo allemaal',NULL,NULL,'2018-02-25 11:44:29','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.',NULL),
	(34,'Hallo allemaal',NULL,NULL,'2018-02-25 11:44:30','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.',NULL),
	(35,'Hallo allemaal2',NULL,NULL,'2018-02-25 11:44:38','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.',NULL),
	(36,'Hallo allemaal2',NULL,NULL,'2018-02-25 11:46:27','Curabitur non velit mi. Duis sed sagittis odio, eget tristique nulla. Donec at nisi ac velit sollicitudin commodo. Vestibulum dictum id metus vel euismod. Quisque rutrum ligula sagittis turpis commodo, ut facilisis lacus imperdiet. Fusce elit mauris, egestas scelerisque bibendum at, iaculis eget dui. Maecenas vitae tellus ornare, vehicula mauris non, sodales elit. Integer sit amet nulla ut nisl mollis finibus.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum consequat mi ut tincidunt. Nullam accumsan tincidunt enim a bibendum. Nullam accumsan turpis id dui aliquet molestie. Etiam velit mi, semper gravida rhoncus laoreet, ultricies sit amet mauris. Vestibulum erat turpis, dapibus at consectetur sit amet, vehicula ac turpis. Aliquam viverra gravida justo, at imperdiet orci venenatis id. Proin dolor sapien, aliquam vel elit quis, porttitor molestie lorem. Proin maximus metus ac augue commodo, ultrices imperdiet sem dignissim. Donec et sem magna.',NULL);

/*!40000 ALTER TABLE `nieuwsberichten` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table schrijvers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schrijvers`;

CREATE TABLE `schrijvers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL DEFAULT '',
  `wachtwoord` char(32) NOT NULL DEFAULT '',
  `telefoon` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schrijvers` WRITE;
/*!40000 ALTER TABLE `schrijvers` DISABLE KEYS */;

INSERT INTO `schrijvers` (`id`, `naam`, `wachtwoord`, `telefoon`, `email`)
VALUES
	(1,'me','371620aa75830b1388b63305b0d42f06','','jaaaaaa@me.me'),
	(2,'ik','a4ef76c127ea3fc9433a0c08a38051de','','jaaaaaa@me.me'),
	(3,'jij','dbfcdd3a1ef5186a3e098332b499070a','','jaaaaaa@me.me'),
	(4,'jij1','dbfcdd3a1ef5186a3e098332b499070a','','new@me.menew@me.menew@me.menew@me.menew@'),
	(5,'jij2','dbfcdd3a1ef5186a3e098332b499070a','','new@me.me'),
	(6,'jij3','dbfcdd3a1ef5186a3e098332b499070a','','dbfcdd3a1ef5186a3e098332b499070a12345678'),
	(7,'jij4','dbfcdd3a1ef5186a3e098332b499070a','','new@me.me'),
	(8,'jij5','dbfcdd3a1ef5186a3e098332b499070a','','new@me.me');

/*!40000 ALTER TABLE `schrijvers` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
