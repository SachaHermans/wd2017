/*Opdracht 1*/
CREATE DATABASE WD314;

/*Opdracht 2*/
INSERT INTO fruit (name, in_box, price, color, weight)
VALUES ('peer', 65, 0.15, 'geel' ,25);

/*Opdracht 3*/
INSERT INTO stock (description, fruit_id, amount)
VALUES ('Conference', 6, 10), ('Doyenne du Comice', 6, 15), ('Durondeau', 6, 20);

/*Opdracht 4*/
SELECT COUNT(description) FROM stock;

/*Opdracht 5*/
SELECT SUM(amount) FROM stock;

/*Opdracht 6*/
SELECT SUM(amount) FROM stock WHERE fruit_id=1;

/*Opdracht 7*/
SELECT name, price FROM fruit
ORDER BY price DESC
LIMIT 1;

/*Opdracht 8*/
SELECT name, price FROM fruit
ORDER BY price ASC
LIMIT 1;

/*Opdracht 9*/
SELECT name AS Naam, stock.description AS Type
FROM fruit
JOIN stock ON fruit.id = stock.fruit_id
ORDER BY name;

/*Opdracht 10*/
SELECT name AS Naam, stock.description AS Type
FROM fruit
JOIN stock ON fruit.id = stock.fruit_id
GROUP BY description
ORDER BY name;

/*Opdracht 11*/
DELETE FROM stock where fruit_id=1;

/*Opdracht 12*/
INSERT INTO stock (description, fruit_id, amount)
VALUES ('Daroyal', 1, 10), ('Christine', 1, 15), ('Darselect', 1, 20);

/*Opdracht 12*/
UPDATE fruit
SET name='kersen'
WHERE name='aardbei';