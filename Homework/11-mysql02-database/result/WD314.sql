# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.35)
# Database: WD314
# Generation Time: 2017-12-10 09:31:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table fruit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fruit`;

CREATE TABLE `fruit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `in_box` int(5) NOT NULL,
  `price` float NOT NULL,
  `color` varchar(15) NOT NULL COMMENT 'De kleur in tekst',
  `weight` decimal(3,0) NOT NULL COMMENT 'Het gewicht van het stuk fruit',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fruit` WRITE;
/*!40000 ALTER TABLE `fruit` DISABLE KEYS */;

INSERT INTO `fruit` (`id`, `name`, `in_box`, `price`, `color`, `weight`)
VALUES
	(1,'kersen',50,0.03,'rood',1),
	(2,'banaan',15,0.25,'geel',15),
	(3,'appel',12,0.15,'geel',25),
	(4,'radijs',40,0.02,'rood',2),
	(5,'meloen',5,1,'groen',100),
	(6,'peer',65,0.15,'geel',25);

/*!40000 ALTER TABLE `fruit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `fruit_id` int(10) NOT NULL,
  `amount` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `description` (`description`),
  KEY `stock_fruit_relatie` (`fruit_id`),
  CONSTRAINT `stock_fruit_relatie` FOREIGN KEY (`fruit_id`) REFERENCES `fruit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;

INSERT INTO `stock` (`id`, `description`, `fruit_id`, `amount`)
VALUES
	(2,'babybanaan',2,50),
	(4,'Granny Smith',3,12),
	(6,'Ronde helderrode',4,12),
	(7,'Granny Smith',3,0),
	(8,'Bakbanaan',2,1),
	(9,'Giant Cavendish',2,6),
	(10,'Cherry Belle',4,12),
	(11,'Rode banaan',2,1),
	(12,'Conference',6,10),
	(13,'Doyenne du Comice',6,15),
	(14,'Durondeau',6,20),
	(15,'Daroyal',1,10),
	(16,'Christine',1,15),
	(17,'Darselect',1,20);

/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
