<?php

/**
 * Class Database Sevice
 *
 * Dit is de database controller
 * Het is de ORM laag tussen mijn project en de database
 */
class DatabaseService
{

    /**
     * @var Object - Database Object
     */
    protected $_mysqli;

    /*
     * maak een database verbinding zodra dit object aangemaakt wordt
     */
    public function __construct()
    {
        $this->_connectToDb();
    }

    /**
     * verbinding maken met mysql
     * geef terug of het wel of niet gelukt is
     *
     * return mysqli object
     */
    private function _connectToDb()
    {
        // zorg ervoor dat er maar één keer een verbinding met de database gemaakt wordt
        if ( ! empty($this->_mysqli)) {
            // geef het mysqli object terug
            return $this->_mysqli;
        }

        $this->_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        // controleer of het gelukt is om een verbinding met de database te maken
        if ($this->_mysqli->connect_errno) {
            die("Failed to connect to MySQL: (" . $this->_mysqli->connect_errno . ") " . $this->_mysqli->connect_error);
        }

        // geef het mysqli object terug
        return $this->_mysqli;
    }

    /**
     * @return mixed
     */
    public function getConnectionObject()
    {
        if (empty($this->_mysqli)) {
            $this->_connectToDb();
        }

        return $this->_mysqli;
    }

    /**
     * @param $query
     *
     * @return mixed
     * @throws \Exception
     */
    public function getResult($query)
    {
        if ( ! $query_statement = $this->_mysqli->prepare($query)) {
            throw new Exception('Prepare failed: (' . $this->_mysqli->errno . ') ' . $this->_mysqli->error . "\n\n" . $query);
        }

        if ( ! $query_statement->execute()) {
            throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
        }

        $result = $query_statement->get_result();

        if (empty($result->num_rows)) {
            throw new Exception('Niets gevonden');
        }

        return $result;
    }


    /**
     * Cleans a string so it is save to use it in a mysql query
     *
     * @param    String $str the data to clean
     *
     * @return string
     */
    function cleanString($str)
    {
        $out = $str;

        // Trim white spaces
        $out = trim($out);

        // remove the union, concat and information schema to prevent hacking
        $out = preg_replace('/(union[\s\+]*?select)|(concat(_ws)?\s*?\()|(information_schema)/i', '', urldecode($out));

        return $out;
    }


}