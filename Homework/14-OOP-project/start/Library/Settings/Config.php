<?php
/**
 * Zorg ervoor dat er maximaal 1 keer is gestart
 */
if ( ! session_id()) {
    session_start();
}

/**
 * Zorg dat alle Errors weergeven worden
 */
ini_set('error_reporting', 1);
error_reporting(E_ALL);


switch ($_SERVER['HTTP_HOST']) {
    case 'cmm-students.localhost' :
        // url ( de host ) ROOT_URL, deze gebruik ik voor mijn css, javascript, images, linkjes in pagina's
        define('ROOT_URL', 'http://cmm-students.localhost/');

        // DOCUMENT_ROOT Het complete pad op je computer naar dit project
        define('DOCUMENT_ROOT', '/Users/teacher/Documents/WebDeveloper Project/');

        // DB instellingen
        define('DB_USER', 'root');
        define('DB_PASS', 'root');
        define('DB_HOST', 'localhost');
        define('DB_NAME', 'cmm_wd315_news');
        break;

    case 'webdeveloper' :
    default:
        define('ROOT_URL', 'http://webdeveloper/');

        define('DOCUMENT_ROOT', '/WebDeveloper/');

        define('DB_USER', 'root');
        define('DB_PASS', 'root');
        define('DB_HOST', 'localhost');
        define('DB_NAME', 'cmm_news');
        break;
}












