<?php
# zorg ervoor dat er altijd een titel is
if ( ! isset( $page_title ) ) {
	$page_title = 'default titel';
}
?><!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

	<title>College of MultiMedia | <?php echo $page_title; ?></title>
	
    <link href="<?php echo ROOT_URL; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo ROOT_URL; ?>css/main.css" rel="stylesheet">
</head>

<body>
	<header>
    <?php include( DOCUMENT_ROOT . 'Library/Views/Main/menu.php' ); ?>
	</header>