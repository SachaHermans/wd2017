	<div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PHP MySQL</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="<?php echo ROOT_URL; ?>about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
                <?php
                # Login Class laden
                require_once( DOCUMENT_ROOT . 'Library/Controllers/LoginController.php' );
                
                $loginClass = new loginController() ;
                $loginClass->showLoginForm();
                ?>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>
    