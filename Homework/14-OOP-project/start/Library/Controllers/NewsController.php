<?php
// include de juiste model documenten
require_once( DOCUMENT_ROOT ."Library/Models/NewsModel.php");


/**
 * Class newsController
 *
 * Mijn newsController die alle functionaliteit van de nieuwsberichten regelt
 * hier handel ik alle database dingen af
 * deze class geeft nieuwsbericht objecten terug
 * de weergave doe ik nu in een view.php
 */
class NewsController
{	
	// zorg ervoor dat de verbinding met de database in deze class opgeslagen wordt
	protected $_db_object;

	/**
	 * Maak een nieuwe nieuwscontroller aan 
	 * en bewaar intern de database
	 */
	 public function __construct( $databaseServiceObject )
	 {
		// bewaar het database object
		// op dit moment bewaar ik het databaseServiceObject in een interne variabele
		$this->_db_object = $databaseServiceObject;
	 }
	 
	 
	 public function showNewsList() {
		$all_news = $this->getAllNews();
		if ( empty( $all_news ) ) {
			return;
		}
		
		include( DOCUMENT_ROOT . 'Library/Views/News/newsList.php' );
	 }
}

