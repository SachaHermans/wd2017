<?php

require_once( '../Library/Settings/Config.php' );

# Maak een database verbinding
# Vanaf het moment dat ik new DatabaseService() aanroep heb ik een verbinding met de database
require_once( DOCUMENT_ROOT . 'Library/Services/DatabaseService.php' );
$databaseService = new DatabaseService();

# Maak een nieuws controller object
require_once( DOCUMENT_ROOT . 'Library/Controllers/NewsController.php' );
$newsController = new NewsController( $databaseService );

# vanaf hier begint mijn weergave van de pagina
# tot hier had ik geen output naar de browser
include( DOCUMENT_ROOT . 'Library/Views/Main/header.php' );

$newsController->showNewsList();

include( DOCUMENT_ROOT . 'Library/Views/Main/footer.php' );
















