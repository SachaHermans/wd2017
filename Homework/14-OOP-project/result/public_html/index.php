<?php
//laden van instellingen en functies

require_once '../Library/Settings/config.php';
require_once CONTROLLERS_PATH . 'functions.php';

$page_active = basename($_SERVER["PHP_SELF"], ".php");

//laden van de header
include_once VIEWS_PATH . 'Main/header.php';

//$title="College of MultiMedia | PHP MySQL";
?>    
    <div class="jumbotron">
        <div class="container">
            <?php   $h1="hello, world!";
                    echo "<h1>" . ucfirst($h1) . "</h1>";
            ?>

            <p>Dit is een template voor een eenvoudige website. <br>
				Het omvat een grote callout genoemd de heldenEenheid en drie ondersteunende stukken of inhoud. Gebruik het as een uitgangspunt om iets</p>

            <p><a href="pages/about.php" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
		<?php
		//include voor het ophalen van de nieuwsberichten
		include_once CONTROLLERS_PATH . 'nieuwsberichten.php';
		?>
	
<?php
		//laden van de footer
		include_once VIEWS_PATH . 'Main/footer.php';