<?php
class DatabaseVerbinding {
	
	protected $_verbinding;
	
	public function __construct()
    {
        $this->connect();
    }

	
//verbinding maken met database

	private function connect(){
 		$this->_verbinding = mysqli_connect ('localhost', 'root', 'root', 'cmm_wd312_nieuwsberichten');
		
		//controle of verbinding gelukt is

		if (mysqli_connect_errno()){
            echo 'Verbinding mislukt met foutmelding: ' . mysqli_connect_error; 
		} 
		return $this->_verbinding;
	}
	
	public function setBericht($query){
		
		//voorbereiden van de query inclusief controle op fouten
		if ( ! $prepare_bericht = $this->_verbinding->prepare($query)){
    		throw new exception('Voorbereiding mislukt: ' . $this->_verbinding->errno . ')' . $this->_verbinding->error);
			}
		
		//uitvoeren van de query inclusief controle op fouten

		if ( ! $prepare_bericht->execute()){
			throw new exception('Uitvoering mislukt: ' . $this->_verbinding->errno . ')' . $this->_verbinding->error);
		}	 
	}
}