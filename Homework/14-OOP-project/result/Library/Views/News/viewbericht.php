<?php
$newLine = "<br>\n";
$bericht = mysqli_fetch_assoc($prepare_result);
	
if (!isset($_SESSION['ingelogd'])){ 
	echo '<h2>' . $bericht['titel'] . '</h2>' . $newLine;
	echo '<p>' . $bericht['content'] . '</p>'. $newLine;
	echo '<p><a class="btn btn-default" href="../index.php">Home &raquo;</a></p>';
	// echo "<a href='../index.php'>Home &raquo;</a>";
} else { ?>
		<br>
		<form method="post">
		<div class="form-group">
				<label for="titel">Titel:</label>
				<input type="text" class="form-control" name="titel" value="<?php echo $bericht['titel'] ?>"><br>
		</div>
		<div class="form-group">
				<label for="content">Content:</label>
				<textarea class="form-control" rows="20" name="content"><?php echo $bericht['content'] ?></textarea><br>
		</div>
		<div class="form-group">
				<a class="btn btn-danger" href="../index.php">Annuleer</a>
				<!--<button type="" class="btn btn-danger">Annuleer</button>-->
				<button type="submit" class="btn btn-success">Opslaan</button><br>
		</div>
		</form> <?php 
}