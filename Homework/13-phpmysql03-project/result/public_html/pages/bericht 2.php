<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// declaratie van de variabele voor het geven van een return
$new_line = "<br>\n";
$titel = "<h2>Het Fluitketeltje</h2>";
$tekst = "<p>
Meneer is niet thuis en mevrouw is niet thuis,<br>
het keteltje staat op het kolenfornuis,<br>
de hele familie is uit,<br>
en het fluit en het fluit en het fluit: túúúút.<br><br>
De pan met andijvie zegt: Foei, o, foei!<br>
Hou eindelijk op met dat nare geloei!<br>
Wees eindelijk stil asjeblief,<br>je lijkt wel een locomotief.<br><br>
De deftige braadpan met lapjes en zjuu<br>
zegt: Goeie genade, wat krijgen we nu?<br>
Je kunt niet meer sudderen hier,<br>
ik sudder niet meer met plezier!<br><br>
Het keteltje jammert: Ik hou niet meer op!<br>
Het komt door m'n dop! Het komt door mijn dop!<br>
Ik moet fluiten, zolang als ik kook<br>
en ik kan het niet helpen ook!<br><br>
Meneer en mevrouw zijn nog altijd niet thuis<br>
en het keteltje staat op het kolenfornuis,<br>
het fluit en het fluit en het fluit.<br>
Wij houden het echt niet meer uit... Jullie?</p>";

echo $titel . $tekst . $new_line . $new_line;
echo "<a href='../index.php'>Home &raquo;</a>";