<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// laad de sessie
session_start();

// Definieer constante voor het pad naar URL
define( 'ROOT_PATH', 'http://localhost/' );

// Definieer constante voor het pad naar de projectmap
define('DOCUMENT_ROOT', '/Users/sacha/Documents/CMM/Web_Developer/werkmap_sacha/Project/');

//Definieer constantes naar MVC-paden
define('VIEWS_PATH', DOCUMENT_ROOT . 'Library/Views/');
define('CONTROLLERS_PATH', DOCUMENT_ROOT . 'Library/Controllers/');

//Open de connectie met de database
require_once DOCUMENT_ROOT . 'Library/Services/database.php';