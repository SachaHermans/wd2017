<?php
//verbinding maken met database

$database_verbinding = mysqli_connect ('localhost', 'root', 'root', 'cmm_wd312_nieuwsberichten');

//controle of verbinding gelukt is

if (mysqli_connect_errno()){
            echo 'Verbinding mislukt met foutmelding: ' . mysqli_connect_error; 
} 

//query voor ophalen van nieuwsberichten
$nieuwsbericht_query= 'SELECT id, titel, content
FROM nieuwsberichten
WHERE id=?';

//voorbereiden van de query inclusief controle op fouten

if ( ! $prepare_bericht = $database_verbinding->prepare($nieuwsbericht_query)){
    throw new exception('Voorbereiding mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);
}

    
//Variabele aanmaken voor de id
$berichtId=$_GET['id'];

//id verbinden aan id in database

if ( ! $prepare_bericht->bind_param('i', $berichtId) ){
    throw new exception('Voorbereiding mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);
}


//uitvoeren van de query inclusief controle op fouten

if ( ! $prepare_bericht->execute()){
    throw new exception('Uitvoering mislukt: ' . $database_verbinding->errno . ')' . $database_verbinding->error);
} 
    
//Ophalen van de gegevens

$prepare_result = $prepare_bericht->get_result();

//Controle of er gegevens gevonden zijn

if (empty($prepare_result->num_rows)){
    die('Geen gegevens gevonden');
}