<?php
//functie voor het tonen van berichten
function toon_bericht($bericht_titel, $bericht_omschrijving, $bericht_date){
    $new_line = "<br>\n";
	$bericht_date=strtotime($bericht_date);
	echo '<h2>' . $bericht_titel . '</h2>';
    echo '<p>' . substr($bericht_omschrijving, 0, 500) . '...' . $new_line . $new_line . date("j F Y", $bericht_date) . '</p>';
}

//functie voor de controle of er een gebruiker ingelogd is
function logged_in() {
	if ((''!==$_SESSION['inlog_naam']) || (''!==$_SESSION['inlog_ww']))
		$_SESSION['ingelogd']=true;
}