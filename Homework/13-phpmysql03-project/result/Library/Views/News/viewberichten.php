<?php
$teller = 0;
echo '<div class="row">';
while ($berichten = mysqli_fetch_assoc($prepare_result)){
	if (($teller%3 == 0) && ($teller > 1)){
		echo '</div><hr><div class="row">';
	}
	echo '<div class="col-lg-4">';
	toon_bericht($berichten['titel'], $berichten['content'], $berichten['publicatie_datum']);
	echo '<p><a class="btn btn-default" href="/pages/bericht.php?id=' . $berichten['id'] . '">View details &raquo;</a></p>';
	echo '</div>';
	$teller++;
}
echo '</div>';
if ($teller%3 != 0) {
	echo '<hr>';
	}