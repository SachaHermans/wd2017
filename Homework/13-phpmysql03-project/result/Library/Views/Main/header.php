<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    
    <title>College of MultiMedia | <?php echo strtolower(str_replace(' ','',$page_active))?></title>

    <link href="<?=constant('ROOT_PATH')?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=constant('ROOT_PATH')?>css/main.css" rel="stylesheet">
</head>
<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PHP MySQL</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="<?=($page_active=='index') ? 'active' : ''?>"><a href="<?=constant('ROOT_PATH')?>index.php">Home</a></li>
                    <li class="<?=($page_active=='about') ? 'active' : ''?>"><a href="<?=constant('ROOT_PATH')?>pages/about.php">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
			<?php include VIEWS_PATH . 'Login/login.php'?>
           </div>
            <!--/.navbar-collapse -->
        </div>
    </div>