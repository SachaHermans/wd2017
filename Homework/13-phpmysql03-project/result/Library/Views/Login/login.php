<?php
// declaratie van de variabele voor het geven van een return
$newLine = "<br>\n";

if (isset($_POST['inlog_naam']) && ($_POST['inlog_ww'])) {
	$_SESSION['inlog_naam']=($_POST['inlog_naam']);
	$_SESSION['inlog_ww']=($_POST['inlog_ww']);
	logged_in();
}

if (isset($_POST['submit'])){
	session_unset();
	session_destroy();
}

if (isset($_SESSION['ingelogd']) && 'true'==($_SESSION['ingelogd'])){
	echo '<form class="navbar-form navbar-right" action="" method="post">';
	echo '<div style="color:red">' . $_SESSION['inlog_naam'] . '</div><input type="hidden" name="submit" value="submit" /><button type="submit" class="btn btn-success">Sign out</button>';
	echo '</form>';
} else {
	echo '<form class="navbar-form navbar-right" action="" method="post">';
	echo '<div class="form-group">';
    echo '<input type="text" name="inlog_naam" placeholder="Email" class="form-control">';
    echo '</div>';
    echo '<div class="form-group">';
    echo '<input type="password" name="inlog_ww" placeholder="Password" class="form-control">';
    echo '</div>';
	echo '<button type="submit" class="btn btn-success">Sign in</button>';
	echo '</form>';
}