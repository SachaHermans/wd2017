<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

// variabele om doelbestand te overschrijven
$doel_file="images/volkswagen.jpg";

if(isset($_POST["submit"])) {
	//variabele om bronbestandsnaam te verkrijgen
	$bron_file=basename($_FILES["upload_bestand"]["name"]);
	//variabelen voor het verkrijgen van de extensie van de file
	$check_file = new SplFileInfo($bron_file);
	$extensie=($check_file->getExtension());
	
	if (($extensie!=="jpg") && ($extensie!=="png"))
		echo "Bestand moet een .jpg of .png zijn";
	else
		move_uploaded_file($_FILES['upload_bestand']['tmp_name'], $doel_file);
	if ($_FILES['upload_bestand']['error']===UPLOAD_ERR_OK)
		echo "Het vervangen van de foto was succesvol";
	else
		echo "Het vervangen van de foto is mislukt. Probeer het nog een keer";
}
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Formulier</title>

</head>

<body>
	<div>
		<form action="form.php" method="post" enctype="multipart/form-data">
  			Selecteer het bestand voor uploaden:<br>
  			<input type="file" name="upload_bestand" id="upload_bestand"><br>
    		<input type="submit" value="Verstuur" name="submit"><br>
 		</form>
	</div>
</body>
</html>