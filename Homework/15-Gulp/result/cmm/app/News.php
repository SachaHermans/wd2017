<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	public function scopeActive($query, $slug)
	{
		return $query->where(
			[
				['publish_date+', '<=',date('Y-m-d')],
				['slug', '=',$slug],
			])->firstOrFail();
		}


	protected $guarded = ['user_id'];
    /**
     * @return bool
     */
    public function scopeLatest($query, $limit)
    {
        return $query->orderBy('publish_date', 'desc')
                     ->where('publish_date', '<=', date('Y-m-d', time()))
                     ->limit($limit)->get();
    }
}
