<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

/*
 * News routes
 */
Route::get('/news', 			'NewsController@index');
Route::get('/news/create', 		'NewsController@create')->middleware('auth');
Route::get('/news/{slug}', 		'NewsController@show');
Route::get('/news/{news}/edit', 'NewsController@edit')->middleware('auth');
Route::post('/news', 			'NewsController@store')->middleware('auth');
Route::post('/news/{news}', 	'NewsController@update')->middleware('auth');
Route::delete('/news/{news}', 	'NewsController@delete')->middleware('auth');



/*
 * Page routes
 */
Route::get('/', 'PagesController@index');
Route::get('/profiel', 'PagesController@profile')->middleware('auth')->name('profile');
Route::get('/profile', 'PagesController@profile')->middleware('auth')->name('profile');
Route::get('/{page}', 'PagesController@show');



