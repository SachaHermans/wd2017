var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');

gulp.task('styles', function () {
      return gulp.src('resources/assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('resources/assets/sass/'));
});

gulp.task('javascript', function () {
    gulp.src('resources/assets/js/**/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/js'));
});


gulp.task('minify', function () {
    gulp.src('resources/assets/sass/**/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('default', function(){
    gulp.watch('resources/assets/sass/**/*.scss', ['styles']);
    gulp.watch('resources/assets/sass/**/*.css', ['minify']);
    gulp.watch('resources/assets/js/**/*.js', ['javascript']);
});
