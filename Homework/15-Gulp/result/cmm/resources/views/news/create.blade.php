@extends( 'layout.master' )

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>Maak een nieuw bericht</h1>
            
@include( 'layout.partials.errors') 
            
            <form method="post" action="/news">
            {{csrf_field()}}
            
			  <div class="form-group">
				<label for="title">Titel</label>
				<input type="text" class="form-control" name="title" placeholder="Titel" value="{{$news->title}}" required>
			  </div>
			  
			  <div class="form-group">
				<label for="publish_date">Publicatie datum</label>
				<input type="text" class="form-control" name="publish_date" placeholder="yyyy-mm-dd" value="{{$news->publish_date}}"  required>
			  </div>
			  
			  <div class="form-group">
				<label for="body">Body</label>
				<textarea class="form-control" name="body" >{{$news->body}}</textarea>
			  </div>
			  
			  <div class="form-group">
				  <button type="submit" class="btn btn-primary">Submit</button>
			  </div>
			</form>
        </div>
    </div>
@endsection







