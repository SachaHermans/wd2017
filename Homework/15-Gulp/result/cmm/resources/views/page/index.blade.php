@extends( 'layout.master' )

@section('content')

    <section class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="jumbotron-heading">{{ $page->title }}</h1>
            <p>
                {{ $page->body }}
            </p>
            <p>
                <a href="/welcome/" class="btn btn-primary" role="button">Welkom</a>
                <a href="/contact/" class="btn btn-primary" role="button">Contact</a>
                <a href="/over_ons/" class="btn btn-primary" role="button">Over ons</a>
                <a href="/news/" class="btn btn-primary" role="button">Nieuws</a>
            </p>
        </div>
    </section>

    <div class="row mb-2">
        @foreach ($messages as $message)
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-120">
                    <div class="card-body d-flex flex-column align-items-start">
                        <h3 class="mb-0">
                            <a href="/news/{{ $message->slug }}">{{ $message->title }}</a>
                        </h3>
                        <div class="mb-1 text-muted">{{ $message->post_date }}</div>
                        <p class="card-text mb-auto">{{ str_limit( $message->body , 50) }}</p>
                        <small class="text-muted"><a href="/news/{{ $message->slug }}">Lees meer</a></small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection