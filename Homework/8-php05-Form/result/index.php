<?php
// laat alle errors zien
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

$new_line = "<br>\n";
$post_naam = $post_bier = '';
$default_naam = 'Naam';
$default_bier = 'Bier';

if( isset($_POST['naam'] ) ) {
	$post_naam 	= trim($_POST['naam'] );
	$post_bier = $_POST['bier'];
    echo $post_naam, $new_line, $post_bier, $new_line, $new_line;
} else {
    echo $default_naam, $new_line, $default_bier, $new_line, $new_line ;
}
?>
<form method="post">
    Naam:<br>
    <input type="text" name="naam" value="<?php echo $post_naam; ?>"><br>
    Welk soort bier:<br>
    <select name="bier">
        
        <?php
        
            $bieren = ['Bokbier','IPA','Triple','Quadruple'];
            foreach( $bieren as $bier ) {
                echo '<option';
		
                if ( $bier == $post_bier ) {
                    echo ' selected="selected"';
                }
                echo '>' . $bier . '</option>';
            }
        ?>
 
    </select>
    <br><br>
    <input type="submit" value="Verstuur">
</form>